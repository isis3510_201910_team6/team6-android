package com.team6moviles.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import com.team6moviles.R
import com.team6moviles.model.*
import com.team6moviles.model.MessageManager.Command
import kotlinx.android.synthetic.main.activity_create_game.*
import java.net.*
import java.util.*


class CreateGameActivity :
		AppCompatActivity() {

	companion object {
		const val USERNAME_EXTRA = "username_extra"
	}

	private lateinit var username: String
	private lateinit var handler: Handler
	private lateinit var serverSocket: ServerSocket

	private var receiveThread: Thread? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_create_game)

		setupViews()
		setupHandler()
		setupPlayer()
		showIp()

		setupServer()
		startServer()
	}

	private fun setupViews() {
		setupButtons()
		setupLobbyListView()
	}

	private fun setupButtons() {
		createGameStartButton.setOnClickListener { startGame() }
	}

	private fun setupLobbyListView() {
		createGameLobbyListView.emptyView = createGameLobbyEmptyTextView

	}

	private fun setupHandler() {
		handler = Handler(Handler.Callback { msg: Message ->
			when (msg.what) {
				ConnectivityManager.SOCKET -> {
					val socket: Socket = msg.obj as Socket
					receiveThread = ConnectivityManager.listenForMessages(socket, handler)
				}
				ConnectivityManager.REMOTE_MESSAGE -> {
					val playerMessage: PlayerMessage = msg.obj as PlayerMessage
					processMessage(playerMessage)
				}
			}
			true
		})
	}

	private fun setupPlayer() {
		username = intent.getStringExtra(USERNAME_EXTRA) ?:
				throw IllegalArgumentException("Username extra required")
		ServerGame.player = Player(username)
	}

	private fun showIp() {
		val localIpAddress: String? = localIpAddress
		ipTextView.text = localIpAddress ?: "No ip address found"
	}

	private val localIpAddress: String?
		get() {
			val networkInterfaces: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
			while (networkInterfaces.hasMoreElements()) {
				val networkInterface: NetworkInterface = networkInterfaces.nextElement()
				val ipAddresses: Enumeration<InetAddress> = networkInterface.inetAddresses
				while (ipAddresses.hasMoreElements()) {
					val ipAddress: InetAddress = ipAddresses.nextElement()
					if (!ipAddress.isLoopbackAddress && ipAddress is Inet4Address) {
						return ipAddress.hostAddress
					}
				}
			}
			return null
		}

	private fun setupServer() {
		serverSocket = ServerSocket(ConnectivityManager.PORT)
	}

	private fun startServer() {
		ConnectivityManager.waitForClients(serverSocket, handler)
	}

	private fun startGame() {
		closeAllSockets()
		ServerGame.startGame()
		startActivity(Intent(this, HostPlayActivity::class.java))
	}

	private fun processMessage(playerMessage: PlayerMessage) {
		val message: String = playerMessage.message
		val command: Command = MessageManager.getCommand(message)
		when (command) {
			Command.PLAYER_USERNAME -> {
				onPlayerNameReceived(playerMessage)
			}
		}
	}

	private fun updateLobby() {
		val playersUsernames: Array<String> = ServerGame.playersUsernames.toSet().toTypedArray()
		val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, playersUsernames)
		createGameLobbyListView.adapter = arrayAdapter
	}

	private fun onPlayerNameReceived(playerMessage: PlayerMessage) {
		val playerSocket: Socket = playerMessage.socket
		val playerUsername: String = MessageManager.getPlayerUsername(playerMessage.message)
		Toast.makeText(this, "$playerUsername joined (${playerSocket.inetAddress.hostAddress})", Toast.LENGTH_LONG).show()
		addRemotePlayer(playerUsername, playerSocket)
		updateLobby()
	}

	private fun addRemotePlayer(playerUsername: String, playerSocket: Socket) {
		val player = Player(playerUsername)
		val remotePlayer = RemotePlayer(player, playerSocket)
		ServerGame.addRemotePlayer(remotePlayer)
	}

	private fun closeAllSockets() {
		serverSocket.close()
		receiveThread?.interrupt()
	}

	override fun onDestroy() {
		closeAllSockets()
		super.onDestroy()
	}
}
