package com.team6moviles.ui

import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.preference.PreferenceManager
import com.team6moviles.R
import kotlinx.android.synthetic.main.activity_settings.toolbar

class SettingsActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_settings)

		setupSettingsFragment()
		setupActionBar()
	}

	private fun setupSettingsFragment() {
		supportFragmentManager.beginTransaction()
			.replace(R.id.settings_container, SettingsFragment())
			.commit()
	}

	private fun setupActionBar() {
		setSupportActionBar(toolbar)
		supportActionBar?.title = getString(R.string.settings_activity_title)
	}


	class SettingsFragment : PreferenceFragmentCompat(),
		SharedPreferences.OnSharedPreferenceChangeListener {


		private lateinit var  keysNeedSummary : List<String>

		override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
			val preference: Preference = findPreference(key)
			when (key) {
				getString(R.string.controlsKey) -> {
					val controlsPreference = preference as ListPreference
					val displayControl = controlsPreference.entry as String
					controlsPreference.summary = displayControl
				}
				getString(R.string.soundsKey) -> {
					val soundsPreference = preference as ListPreference
					val displayControl = soundsPreference.entry as String
					soundsPreference.summary = displayControl
				}

			}
		}

		override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
			addPreferencesFromResource(R.xml.preferences)
			setSummaries()

		}

		private fun setSummaries() {
			keysNeedSummary = listOf<String>(getString(R.string.controlsKey),getString(R.string.soundsKey))
			for(key in keysNeedSummary){
				val preference =  findPreference(key) as ListPreference
				preference.summary = preference.entry
			}
		}

		override fun onResume() {
			super.onResume()
			preferenceScreen.sharedPreferences
				.registerOnSharedPreferenceChangeListener(this)
		}

		override fun onPause() {
			super.onPause()
			preferenceScreen.sharedPreferences
				.unregisterOnSharedPreferenceChangeListener(this)
		}
	}
}
