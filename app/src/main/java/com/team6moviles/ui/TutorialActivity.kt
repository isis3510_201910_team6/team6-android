package com.team6moviles.ui

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.accessibility.AccessibilityManager
import android.widget.Button
import com.team6moviles.R
import kotlinx.android.synthetic.main.activity_tutorial_non_blind.carouselViewTutorial

class TutorialActivity : AppCompatActivity() {

	private val TAG = javaClass.simpleName

	private var blind = false

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_tutorial_non_blind)

		blind = ((getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager).isTouchExplorationEnabled)


		Log.d(TAG,"is blind: ${blind}")
		var layout = layoutInflater.inflate(R.layout.tutorial_3, null)
		var dismissButton: Button = layout.findViewById(R.id.dismissButton)
		dismissButton.setOnClickListener { this.onBackPressed() }
		carouselViewTutorial.pageCount = 3
		carouselViewTutorial.setViewListener {
			position: Int ->
				when(position){
					0 -> layoutInflater.inflate(R.layout.tutorial_1, null)
					1 -> if(blind) {
						layoutInflater.inflate(R.layout.tutorial_blind_2, null)
					} else layoutInflater.inflate(R.layout.tutorial_nonblind_2, null)
					else -> layout
				}



		}

	}



}
