package com.team6moviles.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.team6moviles.R

class SplashScreen : AppCompatActivity() {

	private val handler = Handler()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_splash_screen)

		handler.postDelayed({
			val intent = Intent(this, WilcomSocketActivity::class.java)
			startActivity(intent)
		},1000)
	}


	override fun onStop() {
		super.onStop()
		handler.removeCallbacks(null)
	}
}
