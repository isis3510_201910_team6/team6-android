package com.team6moviles.ui.ranking

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.team6moviles.R
import kotlinx.android.synthetic.main.item_ranking.view.rankingItemHighscoreTextView
import kotlinx.android.synthetic.main.item_ranking.view.rankingItemPositionTextView
import kotlinx.android.synthetic.main.item_ranking.view.rankingItemUsernameTextView

class PlayerAdapter(options: FirestoreRecyclerOptions<Player>)
		: FirestoreRecyclerAdapter<Player, PlayerAdapter.PlayerViewHolder>(options) {
	override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PlayerViewHolder {
		val itemView: View = LayoutInflater.from(parent.context)
				.inflate(R.layout.item_ranking, parent, false)
		return PlayerViewHolder(itemView)
	}

	override fun onBindViewHolder(holder: PlayerViewHolder, position: Int, model: Player) {
		holder.bindPlayer(model, position)
	}

	class PlayerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		private val rankingItemUsernameTextView: TextView = itemView.rankingItemUsernameTextView
		private val rankingItemHighscoreTextView: TextView = itemView.rankingItemHighscoreTextView
		private val rankingItemPositionTextView: TextView = itemView.rankingItemPositionTextView

		fun bindPlayer(player: Player, position: Int) {
			rankingItemUsernameTextView.text = player.username
 			if (player.username == null || player.username!!.isEmpty()) {
				with (rankingItemUsernameTextView) {
					text = context.getString(R.string.ranking_user_not_registred)
					setTypeface(typeface, Typeface.ITALIC)
				}
			}
			rankingItemHighscoreTextView.text = player.highscore.toString()
			rankingItemPositionTextView.text = "${position + 1}."

			itemView.contentDescription = itemView.context.getString(R.string.positionTalkBack).format((position+1).toString(), player.username,player.highscore.toString())
		}
	}
}