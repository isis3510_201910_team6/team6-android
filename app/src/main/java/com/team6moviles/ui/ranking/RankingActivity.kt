package com.team6moviles.ui.ranking

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.team6moviles.R
import com.team6moviles.repo.Firestore
import kotlinx.android.synthetic.main.activity_ranking.rankingRecyclerView
import kotlinx.android.synthetic.main.activity_ranking.rankingRootLayout

class RankingActivity : AppCompatActivity() {
	private val playersRef: CollectionReference = FirebaseFirestore.getInstance().collection(Firestore.PLAYERS)
	private lateinit var playerAdapter: PlayerAdapter
	private var isFirstTime = true
	private var isAlreadyConnected = false

	private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			val notConnected = intent.getBooleanExtra(
				ConnectivityManager
					.EXTRA_NO_CONNECTIVITY, false)
			if (notConnected) {
				onDisconnected()
			} else {
				onConnected()
			}
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_ranking)

		setupRecyclerView()
	}

	private fun setupRecyclerView() {
		val query: Query = playersRef.orderBy(Firestore.HIGH, Query.Direction.DESCENDING)
			.limit(100)
		val options: FirestoreRecyclerOptions<Player> = FirestoreRecyclerOptions.Builder<Player>()
			.setQuery(query, Player::class.java)
			.build()
		playerAdapter = PlayerAdapter(options)
		with (rankingRecyclerView) {
			layoutManager = LinearLayoutManager(this@RankingActivity)
			adapter = playerAdapter
		}
		playersRef.addSnapshotListener(EventListener<QuerySnapshot> {
			snapshots, e ->
			if (e != null) {
				return@EventListener
			}

			if (snapshots != null) {
				playerAdapter.notifyDataSetChanged()
			}
		})
	}

	private fun onConnected() {
		if (!isFirstTime && !isAlreadyConnected) {
			Snackbar.make(rankingRootLayout, getString(R.string.back_online), Snackbar.LENGTH_LONG).show()
			isAlreadyConnected = true
		}
	}

	private fun onDisconnected() {
		val snackbar = Snackbar.make(rankingRootLayout, getString(R.string.ranking_outdated), Snackbar.LENGTH_INDEFINITE)
		snackbar.setAction("OK", View.OnClickListener {
			snackbar.dismiss()
		})
		snackbar.show()
		isFirstTime = false
		isAlreadyConnected = false
	}

	override fun onStart() {
		playerAdapter.startListening()
		registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
		super.onStart()
	}

	override fun onStop() {
		playerAdapter.stopListening()
		unregisterReceiver(broadcastReceiver)
		super.onStop()
	}
}
