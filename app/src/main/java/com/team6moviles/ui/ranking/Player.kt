package com.team6moviles.ui.ranking

class Player {
	var username: String? = null
	var highscore: Int? = null

	constructor()

	constructor(username: String, highscore: Int) {
		this.username = username
		this.highscore = highscore
	}
}