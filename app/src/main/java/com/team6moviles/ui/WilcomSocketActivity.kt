package com.team6moviles.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.hardware.input.InputManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.team6moviles.R
import com.team6moviles.repo.Firestore
import com.team6moviles.repo.LocalStorage
import kotlinx.android.synthetic.main.activity_wilcom_socket.wilcomSocketRootLayout
import kotlinx.android.synthetic.main.activity_wilcom_socket.wilcomSocketSignUpButton
import kotlinx.android.synthetic.main.activity_wilcom_socket.wilcomSocketSkipButton
import kotlinx.android.synthetic.main.activity_wilcom_socket.wilcomSocketUsernameEditText

class WilcomSocketActivity : AppCompatActivity() {
	private lateinit var firestore: Firestore
	private var firstTime = true

	private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			val notConnected = intent.getBooleanExtra(
				ConnectivityManager
				.EXTRA_NO_CONNECTIVITY, false)
			if (notConnected) {
				disconnected()
			} else {
				connected()
			}
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		if (!userExists()) {
			setContentView(R.layout.activity_wilcom_socket)
			setupViews()
			setupFirestore()
		}
	}

	override fun onStart() {
		super.onStart()
		registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
	}

	override fun onStop() {
		super.onStop()
		unregisterReceiver(broadcastReceiver)
	}

	private fun userExists(): Boolean {
		if (LocalStorage.cachePersistedUserId(this)) {
			startMenuActivity()
			return true
		}
		return false
	}

	private fun setupViews() {
		wilcomSocketSignUpButton.setOnClickListener {
			if (wilcomSocketSignUpButton.isEnabled){
				disableButtons()
				(wilcomSocketSignUpButton as View).hideKeyboard()
				Snackbar.make(wilcomSocketRootLayout, getString(R.string.wilcom_socket_wait), Snackbar.LENGTH_LONG).show()
				trySignIn()

			}else{
				Snackbar.make(wilcomSocketRootLayout, getString(R.string.wilcom_socket_cant_setup_game), Snackbar.LENGTH_LONG).show()
			}
		}
		wilcomSocketSkipButton.setOnClickListener {
			if (wilcomSocketSkipButton.isEnabled){
				disableButtons()
				processPlayer()
			}else{
				Snackbar.make(wilcomSocketRootLayout, getString(R.string.wilcom_socket_cant_setup_game), Snackbar.LENGTH_LONG).show()
			}
		}
	}

	fun View.hideKeyboard() {
		val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		imm.hideSoftInputFromWindow(windowToken, 0)
	}

	private fun setupFirestore() {
		firestore = Firestore()
	}

	private fun trySignIn() {
		val username: String = wilcomSocketUsernameEditText.text.toString()
		if (username.isNotEmpty()) {
			processPlayer(username)
		} else {
			enableButtons()
			Snackbar.make(wilcomSocketRootLayout, getString(R.string.wilcom_socket_enter_username), Snackbar.LENGTH_LONG).show()
		}
	}

	private fun processPlayer(username: String = "") {

		if(username.isEmpty()){
			createPlayer(username)
		}else {
			firestore.foundPlayer(username) { found ->
				if (found) {
					enableButtons()
					Snackbar.make(wilcomSocketRootLayout, getString(R.string.wilcom_socket_username_in_use), Snackbar.LENGTH_LONG).show()
				} else {
					createPlayer(username)
				}
			}
		}
	}


	private fun createPlayer(username: String){
		firestore.createPlayer(this, username) {
				success: Boolean ->
			if (success) {
				startMenuActivity()
			}
		}
	}

	private fun startMenuActivity() {
		val intent = Intent(this, MGMenuActivity::class.java)
		startActivity(intent)
	}

	private fun connected() {
		if (!firstTime){
			Snackbar.make(wilcomSocketRootLayout, getString(R.string.back_online), Snackbar.LENGTH_LONG).show()
			enableButtons()
		}
	}

	private fun enableButtons() {
		wilcomSocketSignUpButton.isEnabled = true
		wilcomSocketSignUpButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
		wilcomSocketSkipButton.isEnabled = true
		wilcomSocketSkipButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryLight))

	}


	private fun disconnected() {
		val snackbar = Snackbar.make(wilcomSocketRootLayout, getString(R.string.wilcom_socket_cant_setup_game), Snackbar.LENGTH_INDEFINITE)
		snackbar.setAction("OK", View.OnClickListener {
			snackbar.dismiss()
		})
		snackbar.show()

		firstTime = false

		disableButtons()
	}

	private fun disableButtons() {
		wilcomSocketSignUpButton.isEnabled = false
		wilcomSocketSignUpButton.setBackgroundColor(Color.DKGRAY)
		wilcomSocketSkipButton.isEnabled = false
		wilcomSocketSkipButton.setBackgroundColor(Color.DKGRAY)

	}


}
