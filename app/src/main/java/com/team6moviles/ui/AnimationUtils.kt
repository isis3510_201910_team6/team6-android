package com.team6moviles.ui

import android.animation.Animator

fun Animator.withStartAction(startAction: () -> Unit) {
	this.addListener(object : Animator.AnimatorListener {
		override fun onAnimationRepeat(animation: Animator?) {}
		override fun onAnimationEnd(animation: Animator?) {}
		override fun onAnimationCancel(animation: Animator?) {}
		override fun onAnimationStart(animation: Animator?) {
			startAction()
		}
	})
}

fun Animator.withEndAction(endAction: () -> Unit) {
	this.addListener(object : Animator.AnimatorListener {
		override fun onAnimationRepeat(animation: Animator?) {}
		override fun onAnimationEnd(animation: Animator?) {
			endAction()
		}
		override fun onAnimationCancel(animation: Animator?) {}
		override fun onAnimationStart(animation: Animator?) {}
	})
}