package com.team6moviles.ui

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import com.team6moviles.R
import com.team6moviles.repo.Firestore
import com.team6moviles.repo.LocalStorage
import com.team6moviles.ui.ranking.RankingActivity
import kotlinx.android.synthetic.main.activity_mgmenu.menuHighscoreTextView
import kotlinx.android.synthetic.main.activity_mgmenu.menuMultiplayerButton
import kotlinx.android.synthetic.main.activity_mgmenu.menuPlayImageView
import kotlinx.android.synthetic.main.activity_mgmenu.menuRankingImageView
import kotlinx.android.synthetic.main.activity_mgmenu.menuSettingsImageView
import kotlinx.android.synthetic.main.activity_mgmenu.menuUsernameTextView
import kotlinx.android.synthetic.main.activity_mgmenu.mgMenuRootLayout
import kotlin.system.exitProcess

class MGMenuActivity : AppCompatActivity() {

	private val firestore = Firestore()
	private var loggedIn = false
	private var firstTime = true
	private var username: String? = null
	private val TAG = javaClass.simpleName

	private lateinit var sound: MediaPlayer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mgmenu)

		setupTutorial()
		setupUserData()
        setupViews()
		setupPreferences()
    }

	private fun setupPreferences() {
		PreferenceManager.setDefaultValues(this,R.xml.preferences,true)
//		val sp = PreferenceManager.getDefaultSharedPreferences(this)
//		Log.d(TAG,sp.all.toString())
//		val prefControls = sp.getString(getString(R.string.soundsKey),"ssssssonido")
//		Toast.makeText(this,prefControls,Toast.LENGTH_LONG).show()

	}

	private fun setupTutorial() {
		firstTime = !LocalStorage.userLoggedIn(this)
		Log.d(TAG,"Is first time, ${firstTime}")
		if(firstTime){
			showTutorial()
			LocalStorage.logInSharedPreferences(this)
		}else return
	}

	private fun showTutorial() {
			val intent = Intent(this, TutorialActivity::class.java)
			startActivity(intent)
	}

	private fun setupUserData() {
		firestore.getUserDocumentData {
			score: Int, username: String ->
			setScore(score)
			if(!loggedIn)
				setUsername(username)
			this.username = if (username.isNotEmpty()) username else null
		}
	}

	private fun setUsername(username: String) {
		menuUsernameTextView.text = username
		menuUsernameTextView.contentDescription = getString(R.string.username).format(username)
		loggedIn = true
	}

	private fun setScore(score: Int) {
		menuHighscoreTextView.text = score.toString()
		menuHighscoreTextView.contentDescription = getString(R.string.menu_high_score).format(score.toString())
	}

	private fun setupViews() {
        menuPlayImageView.setOnClickListener { startPlayActivity() }
		menuRankingImageView.setOnClickListener { startRankingActivity() }
		menuSettingsImageView.setOnClickListener { comingSoonMessage() }
		menuMultiplayerButton.setOnClickListener {
//			startActivity(Intent(this, MultiplayerPlayActivity::class.java))
			startMultiplayerActivity()
		}
		menuSettingsImageView.setOnClickListener { startSettingsActivity() }
    }

	private fun startSettingsActivity() {
		val intent = Intent(this, SettingsActivity::class.java)
		startActivity(intent)
	}

	private fun comingSoonMessage() {
		Toast.makeText(this, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
	}

	private fun startPlayActivity() {
        val intent = Intent(this, MGPlayActivity::class.java)
        startActivity(intent)
    }

	private fun startRankingActivity() {
		val intent = Intent(this, RankingActivity::class.java)
		startActivity(intent)
	}

	private fun startMultiplayerActivity() {
		if (username == null) {
			Snackbar.make(mgMenuRootLayout, "You need to be logged in", Snackbar.LENGTH_INDEFINITE).apply {
				setAction("OK") {
					dismiss()
				}
				show()
			}
		} else {
			val intent = Intent(this, MPMenuActivity::class.java).apply {
				putExtra(MPMenuActivity.USERNAME_EXTRA, username)
			}
			startActivity(intent)
		}
	}

	override fun onBackPressed() {
//		Log.d("onBackPressed","User id: " + LocalStorage.userId)
//		if(LocalStorage.userId.isEmpty()){
//			super.onBackPressed()
//		}

//		var baseInputConnection = BaseInputConnection(menuHighscoreTextView,true)
//		baseInputConnection.sendKeyEvent(KeyEvent(KeyEvent.KEYCODE_HOME, KeyEvent.KEYCODE_POUND))
		moveTaskToBack(true)
		exitProcess(-1)
	}

	override fun onResume() {

		setupUserData()
		super.onResume()
	}
}
