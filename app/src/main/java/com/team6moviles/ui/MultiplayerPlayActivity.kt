package com.team6moviles.ui

import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.team6moviles.R

class MultiplayerPlayActivity : AppCompatActivity(),
	MultiplayerPlayFragment.FragmentInteractionListener {

	private lateinit var endGameFragment: MultiplayerEndGameFragment

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_multiplayer_play)

		setFragment()
	}

	private fun setFragment() {
		endGameFragment = MultiplayerEndGameFragment()
		supportFragmentManager.beginTransaction().apply {
			setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
			replace(R.id.fragmentContainer, endGameFragment)
			commit()
		}
	}

	override fun onScoreUpdate(score: Int) {
		Toast.makeText(this, "New score: $score", Toast.LENGTH_SHORT).show()
	}

	override fun onGameOver() {
		Toast.makeText(this, "Game over", Toast.LENGTH_LONG).show()
	}
}
