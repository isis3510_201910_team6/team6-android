package com.team6moviles.ui

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.*
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.support.v4.view.GestureDetectorCompat
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.Toast
import com.github.nisrulz.sensey.Sensey
import com.github.nisrulz.sensey.ShakeDetector
import com.team6moviles.R
import com.team6moviles.model.Action
import com.team6moviles.model.MemoryGame
import com.team6moviles.model.Player
import com.team6moviles.model.PlayersUtils
import kotlinx.android.synthetic.main.fragment_multiplayer_play.*
import java.util.Locale
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.isNotEmpty
import kotlin.collections.mutableListOf
import kotlin.collections.set
import kotlin.collections.toFloatArray

class MultiplayerPlayFragment : Fragment(),
	TextToSpeech.OnInitListener {

	interface FragmentInteractionListener {
		fun onScoreUpdate(score: Int)
		fun onGameOver()
	}

	private var listener: FragmentInteractionListener? = null

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_multiplayer_play, container, false)
	}

	override fun onAttach(context: Context) {
		super.onAttach(context)
		listener = context as? FragmentInteractionListener
	}

	override fun onDetach() {
		super.onDetach()
		listener = null
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		setupGestureDetector()
		setupTts()
		setupMemoryGame()
		setupButtons()
		setupSounds()
	}

	companion object {
		private const val REQUEST_CODE_CHECK_TTS = 1
	}

	private val TAG = MultiplayerPlayFragment::class.java.simpleName

	//	private lateinit var flipListener: FlipDetector.FlipListener
	private lateinit var shakeListener: ShakeDetector.ShakeListener
	private var timeLastAction = 0L

	private val currentTiemstamp: Long
		get() = System.currentTimeMillis() / 1000L
	private var isWatingForDoubleTap = false

	private var initialTimestamp: Long = 0L

	private lateinit var gestureDetector: GestureDetectorCompat
	private lateinit var tts: TextToSpeech
	private lateinit var sensey: Sensey

	private val handler = Handler()

	private lateinit var swipeSound: MediaPlayer
	private lateinit var tapSound: MediaPlayer
	private lateinit var successSound: MediaPlayer
	private lateinit var gameOverSound: MediaPlayer
	private lateinit var shakeSound: MediaPlayer
	private lateinit var faceDownSound: MediaPlayer


	private val instructionQueue: MutableList<Action> = mutableListOf()
	private lateinit var memoryGame: MemoryGame
	private var isExecuting = false
	private var utteranceIsInstruction = true
	private var actionToAnimate: Action? = null

//	Setup

	private fun setupGestureDetector() {

		gestureDetector = GestureDetectorCompat(activity, object : GestureDetector.SimpleOnGestureListener() {
			override fun onDown(e: MotionEvent): Boolean {
				return true
			}

			override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
				val x1: Float = e1.x
				val y1: Float = e1.y

				val x2: Float = e2.x
				val y2: Float = e2.y

				val direction: SwipeInterpreter.Direction = SwipeInterpreter.getDirection(x1, y1, x2, y2)
				when (direction) {
					SwipeInterpreter.Direction.UP -> onActionEvent(Action.SWIPE_UP)
					SwipeInterpreter.Direction.RIGHT -> onActionEvent(Action.SWIPE_RIGHT)
					SwipeInterpreter.Direction.DOWN -> onActionEvent(Action.SWIPE_DOWN)
					SwipeInterpreter.Direction.LEFT -> onActionEvent(Action.SWIPE_LEFT)
				}
				return true
			}

			override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
				onActionEvent(Action.TAP)
				return true
			}

			override fun onDoubleTap(e: MotionEvent?): Boolean {
				onActionEvent(Action.DOUBLE_TAP)
				return true
			}
		})
	}

	fun onPlayersStatusUpdate(players: List<Player>, username: String) {
		val rankingString: String = PlayersUtils.getRankingPositionString(activity!!, players, username) ?: ""
		val leaderString: String = PlayersUtils.getLeaderString(activity!!, players) ?:
				activity!!.getString(R.string.noPlayersFound)
		val playersRemainingString: String = PlayersUtils.getPlayersRemainingString(activity!!, players) ?:
				activity!!.getString(R.string.noPlayersRemaining)
		rankingPositionTextView.text = rankingString
		bestPlayerTextView.text = leaderString
		playersRemainingTextView.text = playersRemainingString
	}

	fun onTouchEvent(event: MotionEvent): Boolean {
		return gestureDetector.onTouchEvent(event)
	}

	private fun setupMemoryGame() {
		val numberOfActions = 6
		memoryGame = MemoryGame(numberOfActions)
	}

	private fun setupButtons() {
		testButton.setOnClickListener { onTestButtonClick() }
	}

	private fun setupSounds() {
		swipeSound = MediaPlayer.create(activity, R.raw.swipe)
		shakeSound= MediaPlayer.create(activity, R.raw.swipe)
		faceDownSound = MediaPlayer.create(activity, R.raw.swipe)
		tapSound = MediaPlayer.create(activity, R.raw.tap).apply {
			setOnCompletionListener {
				if (isWatingForDoubleTap) {
					isWatingForDoubleTap = false
					handler.postDelayed({ start() }, 150L)
				}
			}
		}
		successSound = MediaPlayer.create(activity, R.raw.success)
		gameOverSound = MediaPlayer.create(activity, R.raw.game_over).apply {
			setOnCompletionListener { sayText(getString(R.string.game_over_utterance), false) }
		}
	}

	private fun saySequence() {
		isExecuting = false
		buildSequenceQueue()
		val waitingTimeForInstructions = 700L
		handler.postDelayed({
			sayNextInstruction()
		}, waitingTimeForInstructions)
	}

	private fun buildSequenceQueue() {
		with (instructionQueue) {
			clear()
			addAll(memoryGame.actionSequence)
		}
	}

	private fun sayNextInstruction() {
		val nextAction: Action = instructionQueue.removeAt(0)
		Log.d(TAG, "i: $nextAction")
		val nextCommand: String = getString(nextAction.commandResId)
		actionToAnimate = nextAction
		sayText(nextCommand, true)
	}

	private fun onFinishInstructions() {
		isExecuting = true
	}

	private fun executeAction(action: Action) {
		Log.d(TAG, "e: $action")
		if(memoryGame.isGameOver) return
		val isCorrect: Boolean = memoryGame.executeAction(action) ?: return
		if (isCorrect) {
			onCorrectGuess(action)
		} else {
			finishGame()
		}
	}

	private fun onCorrectGuess(action: Action) {
		animateAction(action, false)
	}

	private fun dismissForTime(): Boolean {
		return if (timeLastAction != 0L) {
			val currentTime = System.currentTimeMillis()
			val timeDiff = currentTime - timeLastAction
			if (timeDiff<1000L) {
				true
			} else {
				timeLastAction = currentTime
				false
			}
		} else {
			timeLastAction = System.currentTimeMillis()
			false
		}
	}

	private fun finishGame() {
		isExecuting = false
		vibrate()
		playGameOverSound()
		stopSensey()
	}

	private fun onGameOver() {
		listener?.onGameOver()
	}

	private fun stopSensey() {
//		Stop Sensey
		sensey.stopShakeDetection(shakeListener)
//		sensey.stopFlipDetection(flipListener)
		sensey.stop()
	}

	private fun onTestButtonClick() {
		animateRestart()
	}

	private fun updateScoreView() {
		val score: Int = memoryGame.score
		val updateTextView: () -> Unit = { scoreTextView.text = score.toString() }
		if (score == 1) {
			val scaleAnimator = createScaleScoreAnimator(true)
			with(scaleAnimator) {
				withStartAction(updateTextView)
				start()
			}
		} else {
			updateTextView()
		}
	}

	private fun restart() {
		memoryGame.reset()
		updateScoreView()
		startGame()
	}

	private fun startGame() {
		listenSensey()
		saySequence()
		initialTimestamp = currentTiemstamp

	}

	private fun listenSensey() {
		sensey = Sensey.getInstance()
		sensey.init(activity, Sensey.SAMPLING_PERIOD_UI)


		shakeListener = object : ShakeDetector.ShakeListener {
			override fun onShakeDetected() {
				// Shake detected, do something
				Log.d(TAG,"Shake detected")
				if (dismissForTime()) {
					Log.d(TAG,"shake dismissed")
					return
				}
				onActionEvent(Action.SHAKE)
			}
			override fun onShakeStopped() {
				// Shake stopped, do nothing
			}
		}
		sensey.startShakeDetection(shakeListener)
//		flipListener = object : FlipDetector.FlipListener {
//			override fun onFaceUp() {
//				// Device Facing up
//			}
//
//			override fun onFaceDown() {
//				// Device Facing down
//				Log.d(TAG,"FaceDown")
//				if(dismissForTime()){
//					Log.d(TAG,"fd dismissed")
//					return
//				}
//				onActionEvent(Action.FACE_DOWN)
//			}
//		}
//
//		sensey.startFlipDetection(flipListener)
	}



	private fun vibrate() {
		val vibrator: Vibrator = activity!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
		val duration = 100L
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
		} else {
			@Suppress("DEPRECATION")
			vibrator.vibrate(duration)
		}
	}

	private fun playSuccessSound() {
		successSound.start()
	}

	private fun playGameOverSound() {
		gameOverSound.start()
	}

	private fun playActionSound(action: Action) {
		val sound: MediaPlayer = when (action) {
			Action.SWIPE_UP -> swipeSound
			Action.SWIPE_RIGHT -> swipeSound
			Action.SWIPE_DOWN -> swipeSound
			Action.SWIPE_LEFT -> swipeSound
			Action.TAP -> tapSound
			Action.DOUBLE_TAP -> {
				isWatingForDoubleTap = true
				tapSound
			}
			Action.SHAKE -> shakeSound
			Action.FACE_DOWN -> faceDownSound
		}
		sound.start()
	}

	//	Animations

	private fun animateAction(action: Action, isInstruction: Boolean) {
		val actionAnimation: Animator = when (action) {
			Action.SWIPE_UP, Action.SWIPE_RIGHT, Action.SWIPE_DOWN, Action.SWIPE_LEFT -> {
				createSwipeAnimator(action)
			}
			Action.TAP, Action.DOUBLE_TAP -> {
				createTapAnimator(action)
			}
            Action.SHAKE -> {
                createShakeAnimator()
            }
			Action.FACE_DOWN ->{
				createSwipeAnimator(Action.SWIPE_DOWN)
			}
		}
		if (!isInstruction) {
			actionAnimation.withEndAction {
				if (memoryGame.isReadyForNextSequence) {
					onCompletedSequence()
				}
			}
		}
		actionAnimation.start()
	}

	private fun onCompletedSequence() {
		updateScoreView()
		playSuccessSound()
		saySequence()
		listener?.onScoreUpdate(memoryGame.score)
	}

	private fun createSwipeAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.SWIPE_UP -> swipeUpCircleView
			Action.SWIPE_RIGHT -> swipeRightCircleView
			Action.SWIPE_DOWN -> swipeDownCircleView
			Action.SWIPE_LEFT -> swipeLeftCircleView
			else -> throw IllegalArgumentException("Only swipe actions allowed: $action")
		}
		val maxFadeValue = 0.8f
		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, maxFadeValue, 0f)
		var translationInDps = 50f
		if (action == Action.SWIPE_LEFT || action == Action.SWIPE_UP) {
			translationInDps *= -1
		}
		var translationProperty = "translationX"
		if (action == Action.SWIPE_UP || action == Action.SWIPE_DOWN) {
			translationProperty = "translationY"
		}
		val translationInPixels = TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP, translationInDps, resources.displayMetrics)
		val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, 0f, translationInPixels)
		return AnimatorSet().apply {
			playTogether(fadeAnimator, translateAnimator)
			duration = 350
		}
	}

    private fun createShakeAnimator(): Animator {
        val animatedView: View = doubleTapCircleView
        val translationInDps = 40f
        val translationInPixels = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, translationInDps, resources.displayMetrics)
        val translationProperty = "translationX"
        var numberOfOscillations = 2
        val transitionSteps: MutableList<Float> = mutableListOf(0f)
        while (numberOfOscillations-- != 0) {
            transitionSteps.add(translationInPixels)
            transitionSteps.add(-translationInPixels)
        }
        transitionSteps.add(0f)
        val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, *transitionSteps.toFloatArray())

        val maxFadeValue = 0.8f
        val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, maxFadeValue, 0f)
        return AnimatorSet().apply {
            playTogether(fadeAnimator, translateAnimator)
            duration = 600
        }
    }

	private fun createScaleScoreAnimator(appear: Boolean): Animator {
		val endScale: Float = if (appear) 1f else 0f
		return ObjectAnimator.ofPropertyValuesHolder(scoreTextView,
			PropertyValuesHolder.ofFloat(View.SCALE_X, endScale),
			PropertyValuesHolder.ofFloat(View.SCALE_Y, endScale))
	}

	private fun createGameOverCirclesAnimator(): Animator {
		return AnimatorSet().apply {
			playTogether(
				createGameOverCircleAnimator(Action.SWIPE_UP),
				createGameOverCircleAnimator(Action.SWIPE_RIGHT),
				createGameOverCircleAnimator(Action.SWIPE_DOWN),
				createGameOverCircleAnimator(Action.SWIPE_LEFT)
			)
		}
	}

	private fun animateRestart() {
		val scoreDisappearAnimator: Animator = createScaleScoreAnimator(false)
		val restartCirclesAnimator: Animator = createRestartCirclesAnimator()
		AnimatorSet().apply {
			playTogether(scoreDisappearAnimator, restartCirclesAnimator)
			withEndAction {
				moveScore(false)
				restart()
			}
			duration = 400
			start()
		}
	}

	private fun moveScore(toCenter: Boolean) {
		val set = ConstraintSet().apply {
			clone(multiplayerPlayConstraintLayout)
			applyTo(multiplayerPlayConstraintLayout)
		}
		if (toCenter) {
			with (set) {
				connect(R.id.scoreTextView, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
				connect(R.id.scoreTextView, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
			}
		} else {
			with (set) {
				connect(R.id.scoreTextView, ConstraintSet.TOP, R.id.topGuideline, ConstraintSet.BOTTOM)
				clear(R.id.scoreTextView, ConstraintSet.BOTTOM)
			}
		}
		set.applyTo(multiplayerPlayConstraintLayout)
	}

	private fun createRestartCirclesAnimator(): Animator {
		return AnimatorSet().apply {
			playTogether(
				createRestartCircleAnimator(Action.SWIPE_UP),
				createRestartCircleAnimator(Action.SWIPE_RIGHT),
				createRestartCircleAnimator(Action.SWIPE_DOWN),
				createRestartCircleAnimator(Action.SWIPE_LEFT)
			)
		}
	}

	private fun createRestartCircleAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.SWIPE_UP -> swipeUpCircleView
			Action.SWIPE_RIGHT -> swipeRightCircleView
			Action.SWIPE_DOWN -> swipeDownCircleView
			Action.SWIPE_LEFT -> swipeLeftCircleView
			else -> throw IllegalArgumentException("Only swipe actions allowed: $action")
		}

		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 1f, 0f)
		val translation = 0f
		var translationProperty = "translationX"
		if (action == Action.SWIPE_UP || action == Action.SWIPE_DOWN) {
			translationProperty = "translationY"
		}
		val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, translation)
		return AnimatorSet().apply {
			playTogether(fadeAnimator, translateAnimator)
		}
	}

	private fun createGameOverCircleAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.SWIPE_UP -> swipeUpCircleView
			Action.SWIPE_RIGHT -> swipeRightCircleView
			Action.SWIPE_DOWN -> swipeDownCircleView
			Action.SWIPE_LEFT -> swipeLeftCircleView
			else -> throw IllegalArgumentException("Only swipe actions allowed: $action")
		}

		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, 1f)
		var translationInDps = 50f
		if (action == Action.SWIPE_LEFT || action == Action.SWIPE_UP) {
			translationInDps *= -1
		}
		var translationProperty = "translationX"
		if (action == Action.SWIPE_UP || action == Action.SWIPE_DOWN) {
			translationProperty = "translationY"
		}
		val translationInPixels = TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP, translationInDps, resources.displayMetrics)
		val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, 0f, translationInPixels)
		return AnimatorSet().apply {
			playTogether(fadeAnimator, translateAnimator)
		}
	}

	private fun createTapAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.TAP -> tapCircleView
			Action.DOUBLE_TAP -> doubleTapCircleView
			else -> throw IllegalArgumentException("Only tap actions allowed: $action")
		}
		val maxFadeValue = 0.8f
		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, maxFadeValue, 0f)

		val initialScale = 0.5f
		val finalScale = 1.5f
		val scaleAnimator = ObjectAnimator.ofPropertyValuesHolder(animatedView,
			PropertyValuesHolder.ofFloat(View.SCALE_X, initialScale, finalScale),
			PropertyValuesHolder.ofFloat(View.SCALE_Y, initialScale, finalScale))

		val singleTapAnimator = AnimatorSet().apply {
			playTogether(fadeAnimator, scaleAnimator)
		}
		val tapAnimator: Animator =  when (action) {
			Action.TAP -> singleTapAnimator
			else -> {
				AnimatorSet().apply {
					playSequentially(singleTapAnimator, singleTapAnimator.clone())
				}
			}
		}
		tapAnimator.duration = 150
		return tapAnimator
	}

//	tts

	private fun setupTts() {
		val checkLanguageResourcesIntent = Intent().apply {
			action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
		}
		startActivityForResult(checkLanguageResourcesIntent, REQUEST_CODE_CHECK_TTS)
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		when (requestCode) {
			REQUEST_CODE_CHECK_TTS -> {
				if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
					tts = TextToSpeech(activity, this)
				} else {
					val installIntent = Intent().apply {
						action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
					}
					startActivity(installIntent)
				}
			}
		}
	}

	override fun onInit(status: Int) {
		val locale: Locale = Locale.getDefault()
		when (tts.isLanguageAvailable(locale)) {
			TextToSpeech.LANG_NOT_SUPPORTED -> {
				Toast.makeText(activity, "${locale.displayLanguage} is not available for text-to-speech", Toast.LENGTH_SHORT).show()
			}
			TextToSpeech.LANG_MISSING_DATA -> {
				Toast.makeText(activity, "Text-to-speech resources are not installed", Toast.LENGTH_SHORT).show()
			}
			else -> {
				tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
					override fun onDone(utteranceId: String?) {
						activity!!.runOnUiThread {
							if (utteranceIsInstruction) {
								if (instructionQueue.isNotEmpty()) {
									sayNextInstruction()
								} else {
									onFinishInstructions()
								}
							} else {
								onGameOver()
							}
						}
					}

					@Suppress("OverridingDeprecatedMember")
					override fun onError(utteranceId: String?) {}

					override fun onStart(utteranceId: String?) {
						activity!!.runOnUiThread {
							if (utteranceIsInstruction && actionToAnimate != null) {
								animateAction(actionToAnimate!!, true)
								actionToAnimate = null
							}
						}
					}
				})
				startGame()
			}
		}
	}

	private fun sayText(command: String, isInstruction: Boolean) {
		utteranceIsInstruction = isInstruction
		val params = HashMap<String, String>()
		params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = command
		@Suppress("DEPRECATION")
		tts.speak(command, TextToSpeech.QUEUE_FLUSH, params)
	}

	private fun stopCommandUtterance() {
		if (::tts.isInitialized) {
			tts.stop()
		}
	}

	private fun removeHandlerCallbacks() {
		handler.removeCallbacksAndMessages(null)
	}

//	Gesture listeners

	private fun onActionEvent(action: Action) {
		if (!isExecuting) return
		playActionSound(action)
		when (action) {
			Action.SWIPE_UP -> onSwipeUp()
			Action.SWIPE_RIGHT -> onSwipeRight()
			Action.SWIPE_DOWN -> onSwipeDown()
			Action.SWIPE_LEFT -> onSwipeLeft()
			Action.TAP -> onTap()
			Action.DOUBLE_TAP -> onDoubleTap()
			Action.SHAKE -> onShake()
			Action.FACE_DOWN -> onFaceDown()
		}
	}

	private fun onSwipeUp() {
		executeAction(Action.SWIPE_UP)
	}

	private fun onSwipeRight() {
		executeAction(Action.SWIPE_RIGHT)
	}

	private fun onSwipeDown() {
		executeAction(Action.SWIPE_DOWN)
	}

	private fun onSwipeLeft() {
		executeAction(Action.SWIPE_LEFT)
	}

	private fun onTap() {
		executeAction(Action.TAP)
	}

	private fun onDoubleTap() {
		executeAction(Action.DOUBLE_TAP)
	}

	private fun onShake() {
		executeAction(Action.SHAKE)
	}

	private fun onFaceDown(){
		executeAction(Action.FACE_DOWN)
	}



//	Activity lifecycle methods

	override fun onStop() {
		stopCommandUtterance()
		removeHandlerCallbacks()
		stopSensey()
		super.onStop()
	}

	override fun onDestroy() {
		with (tts) {
			stop()
			shutdown()
		}
		super.onDestroy()
	}
}
