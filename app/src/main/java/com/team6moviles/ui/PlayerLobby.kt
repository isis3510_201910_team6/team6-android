package com.team6moviles.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import com.team6moviles.R
import com.team6moviles.model.ClientGame
import com.team6moviles.model.ConnectivityManager
import com.team6moviles.model.MessageManager
import com.team6moviles.model.MessageManager.Command
import com.team6moviles.model.PlayerMessage
import kotlinx.android.synthetic.main.activity_player_lobby.*

class PlayerLobby : AppCompatActivity() {

	private lateinit var handler: Handler
	private var receiveThread: Thread? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_player_lobby)

		setupHandler()
		listenForLobbyUpdates()
	}

	private fun setupHandler() {
		handler = Handler(Handler.Callback { msg: Message ->
			when (msg.what) {
				ConnectivityManager.REMOTE_MESSAGE -> {
					val playerMessage = msg.obj as PlayerMessage
					processMessage(playerMessage.message)
				}
			}
			true
		})
	}

	private fun processMessage(message: String) {
		val command: Command = MessageManager.getCommand(message)
		when (command) {
			Command.PLAYERS_USERNAMES -> onPlayersUsernamesReceived(message)
			Command.START_GAME -> startGame()
		}
	}

	private fun onPlayersUsernamesReceived(message: String) {
		val playersUsernames: List<String> = MessageManager.getPlayersUsernames(message)
		udpateLobby(playersUsernames)
	}

	private fun udpateLobby(playersUsernames: List<String>) {
		val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, playersUsernames.toSet().toTypedArray())
		playerLobbyListView.adapter = arrayAdapter
	}

	private fun startGame() {
		receiveThread?.interrupt()
		startActivity(Intent(this, PlayerPlayActivity::class.java))
	}

	private fun listenForLobbyUpdates() {
		receiveThread = ConnectivityManager.listenForMessages(ClientGame.socket, handler)
	}

	override fun onDestroy() {
		super.onDestroy()
		receiveThread?.interrupt()
	}
}
