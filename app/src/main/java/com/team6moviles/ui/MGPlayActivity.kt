package com.team6moviles.ui

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.VibrationEffect
import android.os.Vibrator
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.PreferenceManager
import android.util.Log
import android.util.TypedValue
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.github.nisrulz.sensey.Sensey
import com.github.nisrulz.sensey.ShakeDetector
import com.team6moviles.R
import com.team6moviles.model.Action
import com.team6moviles.model.MemoryGame
import com.team6moviles.repo.Analytics
import com.team6moviles.repo.Firestore
import kotlinx.android.synthetic.main.activity_mgplay.*
import java.lang.IllegalStateException
import java.util.Locale

class MGPlayActivity : AppCompatActivity(),
		TextToSpeech.OnInitListener {

	companion object {
		private const val REQUEST_CODE_CHECK_TTS = 1
	}

//	private lateinit var flipListener: FlipDetector.FlipListener
	private lateinit var shakeListener: ShakeDetector.ShakeListener
	private val TAG = javaClass.simpleName

	private lateinit var sharedPreferences : SharedPreferences
	private val positiveFeedBackSoundKey = "positiveFeedBackSoundKey"
	private val negativeFeedBackVibrationKey = "negativeFeedBackVibrationKey"
	private val backGroundTrackKey = "backGroundTrackKey"
	private var successSoundAllowed = true
	private var failureVibrationallowed = true
	private var backgroundTrackEnabled = false
	private var numberOfControls = 6
	private lateinit var soundtype : String

	private var allSet = false




	private var timeLastAction = 0L


	private val game : HashMap<String,Any>
		get() {
			val gameInstance = HashMap<String,Any>()
			gameInstance["sequence"] = firestore.toIntArray(memoryGame.actionSequence)
			gameInstance["duration"] = duration
			gameInstance["timestamp"] = firestore.currentTimestamp
			gameInstance["score"] = memoryGame.score
			return gameInstance
		}

	private lateinit var firestore: Firestore
	private lateinit var analytics: Analytics

	private val currentTiemstamp: Long
		get() = System.currentTimeMillis() / 1000L
	private var isWatingForDoubleTap = false

	private var initialTimestamp: Long = 0L

	private val duration: Int
		get() = (currentTiemstamp - initialTimestamp).toInt()

	private lateinit var gestureDetector: GestureDetectorCompat
	private lateinit var tts: TextToSpeech
	private lateinit var sensey: Sensey

	private val handler = Handler()


	private lateinit var swipeSound: MediaPlayer
	private lateinit var tapSound: MediaPlayer
	private lateinit var shakeSound: MediaPlayer

	private lateinit var pianoUp: MediaPlayer
	private lateinit var pianoRight: MediaPlayer
	private lateinit var pianoDown: MediaPlayer
	private lateinit var pianoLeft: MediaPlayer
	private lateinit var pianoTap: MediaPlayer
	private lateinit var pianoShake: MediaPlayer

	private lateinit var congaUp: MediaPlayer
	private lateinit var congaRight: MediaPlayer
	private lateinit var congaDown: MediaPlayer
	private lateinit var congaLeft: MediaPlayer
	private lateinit var congaTap: MediaPlayer
	private lateinit var congaShake: MediaPlayer






	private lateinit var backgroundTrack: MediaPlayer
	private lateinit var successSound: MediaPlayer
	private lateinit var gameOverSound: MediaPlayer

	private lateinit var faceDownSound: MediaPlayer


	private val instructionQueue: MutableList<Action> = mutableListOf()
	private lateinit var memoryGame: MemoryGame
	private var isExecuting = false
	private var utteranceIsInstruction = true
	private var actionToAnimate: Action? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_mgplay)

		setupSharedPreferences()
		setupGestureDetector()
		setupTts()
		setupMemoryGame()
		setupButtons()
		setupFirestore()
		setupAnalytics()
		setupSounds()

		allSet=true

	}

	override fun onResume() {



		if(!allSet){
			setupSharedPreferences()
			setupGestureDetector()
			setupSounds()
			allSet=true
		}

		super.onResume()

	}

	private fun setupSharedPreferences() {

		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
		successSoundAllowed = sharedPreferences.getBoolean(positiveFeedBackSoundKey,true)
		Log.d(TAG,"successSoundAllowed $successSoundAllowed ======================================================================================== ")
		failureVibrationallowed = sharedPreferences.getBoolean(negativeFeedBackVibrationKey,true)
		Log.d(TAG,"failureVibrationallowed $failureVibrationallowed ======================================================================================== ")
		backgroundTrackEnabled = sharedPreferences.getBoolean(backGroundTrackKey,false)
		Log.d(TAG,"backgroundTrackEnabled $backgroundTrackEnabled ======================================================================================== ")
		numberOfControls = sharedPreferences.getString(getString(R.string.controlsKey),"6").toInt()
		Log.d(TAG,"numberOfControls $numberOfControls ======================================================================================== ")
		soundtype = sharedPreferences.getString(getString(R.string.soundsKey),"default")
		Log.d(TAG,"soundtype $soundtype ======================================================================================== ")



	}

	private fun setupAnalytics() {
		analytics = Analytics(this)
	}


	private fun setupFirestore() {
		firestore = Firestore()
	}


//	Setup

	private fun setupGestureDetector() {

		gestureDetector = GestureDetectorCompat(this, object : GestureDetector.SimpleOnGestureListener() {
			override fun onDown(e: MotionEvent): Boolean {
				return true
			}

			override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
				val x1: Float = e1.x
				val y1: Float = e1.y

				val x2: Float = e2.x
				val y2: Float = e2.y

				val direction: SwipeInterpreter.Direction = SwipeInterpreter.getDirection(x1, y1, x2, y2)
				when (direction) {
					SwipeInterpreter.Direction.UP -> onActionEvent(Action.SWIPE_UP)
					SwipeInterpreter.Direction.RIGHT -> onActionEvent(Action.SWIPE_RIGHT)
					SwipeInterpreter.Direction.DOWN -> onActionEvent(Action.SWIPE_DOWN)
					SwipeInterpreter.Direction.LEFT -> onActionEvent(Action.SWIPE_LEFT)
				}
				return true
			}

			override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
				onActionEvent(Action.TAP)
				return true
			}

			override fun onDoubleTap(e: MotionEvent?): Boolean {
				onActionEvent(Action.DOUBLE_TAP)
				return true
			}
		})
	}

	override fun onTouchEvent(event: MotionEvent): Boolean {
		return if (gestureDetector.onTouchEvent(event)) {
			true
		} else {
			super.onTouchEvent(event)
		}
	}

	private fun setupMemoryGame() {
		memoryGame = MemoryGame(numberOfControls)
	}

	private fun setupButtons() {
		playAgainImageView.setOnClickListener { animateRestart() }
		homeImageView.setOnClickListener { finish() }
		testButton.setOnClickListener { onTestButtonClick() }
	}

	private fun setupSounds() {

		pianoShake = MediaPlayer.create(this,R.raw.piano_a5)
		pianoUp = MediaPlayer.create(this,R.raw.piano_c5)
		pianoRight = MediaPlayer.create(this,R.raw.piano_d5)
		pianoDown = MediaPlayer.create(this,R.raw.piano_e5)
		pianoLeft = MediaPlayer.create(this,R.raw.piano_f5)
		pianoTap = MediaPlayer.create(this,R.raw.piano_g5)
		
		congaUp = MediaPlayer.create(this,R.raw.up_conga)
		congaRight = MediaPlayer.create(this,R.raw.right_conga)
		congaDown = MediaPlayer.create(this,R.raw.down_conga)
		congaLeft = MediaPlayer.create(this,R.raw.left_conga)
		congaTap = MediaPlayer.create(this,R.raw.tap_conga)
		congaShake = MediaPlayer.create(this,R.raw.shake_conga)





		backgroundTrack = MediaPlayer.create(this, R.raw.tetris)
		backgroundTrack.isLooping = true
		swipeSound = MediaPlayer.create(this, R.raw.swipe)
		shakeSound= MediaPlayer.create(this, R.raw.swipe)
		faceDownSound = MediaPlayer.create(this, R.raw.swipe)
		tapSound = MediaPlayer.create(this, R.raw.tap).apply {
			setOnCompletionListener {
				if (isWatingForDoubleTap) {
					isWatingForDoubleTap = false
					handler.postDelayed({ start() }, 150L)
				}
			}
		}
		successSound = MediaPlayer.create(this, R.raw.success)
		gameOverSound = MediaPlayer.create(this, R.raw.game_over).apply {
			setOnCompletionListener { sayText(getString(R.string.game_over_utterance), false) }
		}
	}

	private fun saySequence() {
		stopExecuting()
		buildSequenceQueue()
		val waitingTimeForInstructions = 700L
		handler.postDelayed({
			sayNextInstruction()
		}, waitingTimeForInstructions)
	}

	private fun stopExecuting() {
		isExecuting = false
		toDarkTheme()

	}


	private fun toLightTheme() {
		mgplayConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
		scoreTextView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
	}


	private fun startExecuting() {
		isExecuting = true
		toLightTheme()
	}

	private fun toDarkTheme() {
		mgplayConstraintLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
		scoreTextView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
	}


	private fun buildSequenceQueue() {
		with (instructionQueue) {
			clear()
			addAll(memoryGame.actionSequence)
		}
	}

	private fun sayNextInstruction() {
		val nextAction: Action = instructionQueue.removeAt(0)
		Log.d(TAG, "i: $nextAction")
		val nextCommand: String = getString(nextAction.commandResId)
		actionToAnimate = nextAction
		sayText(nextCommand, true)

	}

	private fun onFinishInstructions() {

		sayText(getString(R.string.your_turn),false)
		playActivityYourTurn.visibility = View.VISIBLE
		val waitingTimeForInstructions = 700L
		handler.postDelayed({
			playActivityYourTurn.visibility = View.INVISIBLE
			startExecuting()
		}, waitingTimeForInstructions)

	}



	private fun executeAction(action: Action) {
		Log.d(TAG, "e: $action")
		if(memoryGame.isGameOver) return

		val isCorrect: Boolean = memoryGame.executeAction(action) ?: return
		if (isCorrect && action!=Action.SHAKE) {
			animateAction(action, false)
		} else if(isCorrect){
			playActionSound(action)
			animateAction(action,false)
		}else{
			finishGame()
		}
	}

	private fun dismissForTime():Boolean {
		return if(timeLastAction!=0L){
			val currentTime = System.currentTimeMillis()
			val timeDiff = currentTime - timeLastAction
			if(timeDiff<1000L){
				true
			}else{
				timeLastAction = currentTime
				false
			}
		}else{
			timeLastAction = System.currentTimeMillis()
			false
		}
	}

	private fun finishGame() {
		stopExecuting()

		if(backgroundTrackEnabled){
			pauseBackgroundTrack()
		}

		if(failureVibrationallowed){
			vibrate()
		}

		playGameOverSound()

		stopSensey()


		//Update highscore
		updateHighscore()


		//Save game in player game collection
		firestore.addGame(game)


		//Trigger event
		triggerEventFinished()
	}

	private fun pauseBackgroundTrack() {
		backgroundTrack.pause()
	}

	private fun stopSensey() {
		//Stop Sensey
		Log.d(TAG,"Sensey should stoooooooooooooooooooooop")
		if(!::sensey.isInitialized){
			return
		}
		sensey.stopShakeDetection(shakeListener)
//		sensey.stopFlipDetection(flipListener)
		sensey.stop()
	}

	//=============================================================================================
	//TODO CAMBIAR ESTA VUELTA POR ALGO INTELIGENTE ===============================================
	// COMO EN MENU ACTIVITY EL LISTENER ==========================================================
	//=============================================================================================
	private fun triggerEventFinished() {
		firestore.highscore {
			highscore: Int ->
			analytics.triggerGameFinished(game, highscore)
		}
	}

	private fun updateHighscore() {
		firestore.highscore{
			highscore: Int ->
			if (memoryGame.score > highscore) {
				firestore.updateHighscore(memoryGame.score)
			}
		}


	}

	private fun onTestButtonClick() {
		animateRestart()
	}

	private fun updateScoreView() {
		val score: Int = memoryGame.score
		val updateTextView: () -> Unit = { scoreTextView.text = score.toString() }
		if (score == 1) {
			val scaleAnimator = createScaleScoreAnimator(true)
			with(scaleAnimator) {
				withStartAction(updateTextView)
				start()
			}
		} else {
			updateTextView()
		}
	}

	private fun restart() {
		memoryGame.reset()
		updateScoreView()
		startGame()
	}

	private fun startGame() {

		if(backgroundTrackEnabled){
			startBackgroundTrack()
		}
		listenSensey()
		saySequence()
		initialTimestamp = currentTiemstamp

	}

	private fun startBackgroundTrack() {
		backgroundTrack.start()
	}

	private fun listenSensey() {
		sensey = Sensey.getInstance()
		sensey.init(this,Sensey.SAMPLING_PERIOD_UI)


		shakeListener = object : ShakeDetector.ShakeListener {
			override fun onShakeDetected() {
				// Shake detected, do something
				Log.d(TAG,"Shake detected")


				if(dismissForTime()){
					Log.d(TAG,"shake dismissed")
					return
				}

				onActionEvent(Action.SHAKE)


			}

			override fun onShakeStopped() {
				// Shake stopped, do nothing
				Log.d(TAG,"Shake stopped")
			}
		}

		sensey.startShakeDetection(shakeListener)

//		flipListener = object : FlipDetector.FlipListener {
//			override fun onFaceUp() {
//				// Device Facing up
//			}
//
//			override fun onFaceDown() {
//				// Device Facing down
//				Log.d(TAG,"FaceDown")
//				if(dismissForTime()){
//					Log.d(TAG,"fd dismissed")
//					return
//				}
//				onActionEvent(Action.FACE_DOWN)
//			}
//		}
//
//		sensey.startFlipDetection(flipListener)



	}



	private fun vibrate() {
		val vibrator: Vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
		val duration = 100L
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
		} else {
			@Suppress("DEPRECATION")
			vibrator.vibrate(duration)
		}
	}

	private fun playSuccessSound() {

		if(successSoundAllowed){
			successSound.start()
		}
	}

	private fun playGameOverSound() {
		gameOverSound.start()
	}

	private fun playActionSound(action: Action) {
		val sound: MediaPlayer = when (action) {
			Action.SWIPE_UP -> swipeUpSound()
			Action.SWIPE_RIGHT -> swipeRightSound()
			Action.SWIPE_DOWN -> swipeDownSound()
			Action.SWIPE_LEFT -> swipeLeftSound()
			Action.TAP -> getTapSound()
			Action.DOUBLE_TAP -> {
				isWatingForDoubleTap = true
				tapSound
			}
			Action.SHAKE -> getShakeSound()
			Action.FACE_DOWN -> faceDownSound
		} ?: return


		if(sound.isPlaying){
			sound.stop()
			sound.prepare()
		}
		sound.start()
	}

	private fun swipeUpSound(): MediaPlayer?{
		return when(soundtype){
			"default" -> swipeSound
			"piano" -> pianoUp
			"conga"-> congaUp
			"none"-> null
			else -> swipeSound
		}
	}

	private fun swipeRightSound(): MediaPlayer?{
		return when(soundtype){
			"default" -> swipeSound
			"piano" -> pianoRight
			"conga"-> congaRight
			"none"-> null
			else -> swipeSound
		}
	}
	private fun swipeDownSound(): MediaPlayer?{
		return when(soundtype){
			"default" -> swipeSound
			"piano" -> pianoDown
			"conga"-> congaDown
			"none"-> null
			else -> swipeSound
		}
	}
	private fun swipeLeftSound(): MediaPlayer?{
		return when(soundtype){
			"default" -> swipeSound
			"piano" -> pianoLeft
			"conga"-> congaLeft
			"none"-> null
			else -> swipeSound
		}
	}
	private fun getTapSound(): MediaPlayer?{
		return when(soundtype){
			"default" -> tapSound
			"piano" -> pianoTap
			"conga"-> congaTap
			"none"-> null
			else -> tapSound
		}
	}
	private fun getShakeSound(): MediaPlayer?{
		return when(soundtype){
			"default" -> swipeSound
			"piano" -> pianoShake
			"conga"-> congaShake
			"none"-> null
			else -> swipeSound
		}
	}

	//	Animations

	private fun animateAction(action: Action, isInstruction: Boolean) {
		val actionAnimation: Animator = when (action) {
			Action.SWIPE_UP, Action.SWIPE_RIGHT, Action.SWIPE_DOWN, Action.SWIPE_LEFT -> {
				createSwipeAnimator(action)
			}
			Action.TAP, Action.DOUBLE_TAP -> {
				createTapAnimator(action)
			}
            Action.SHAKE -> {
                createShakeAnimator()
            }
			Action.FACE_DOWN -> {
				createSwipeAnimator(Action.SWIPE_DOWN)
			}
		}
		if (!isInstruction) {
			actionAnimation.withEndAction {
				if (memoryGame.isReadyForNextSequence) {
					updateScoreView()
					playSuccessSound()
					saySequence()
				}
			}
		}
		actionAnimation.start()
	}

	private fun createSwipeAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.SWIPE_UP -> swipeUpCircleView
			Action.SWIPE_RIGHT -> swipeRightCircleView
			Action.SWIPE_DOWN -> swipeDownCircleView
			Action.SWIPE_LEFT -> swipeLeftCircleView
			else -> throw IllegalArgumentException("Only swipe actions allowed: $action")
		}
		val maxFadeValue = 0.8f
		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, maxFadeValue, 0f)
		var translationInDps = 100f
		if (action == Action.SWIPE_LEFT || action == Action.SWIPE_UP) {
			translationInDps *= -1
		}
		var translationProperty = "translationX"
		if (action == Action.SWIPE_UP || action == Action.SWIPE_DOWN) {
			translationProperty = "translationY"
		}
		val translationInPixels = TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP, translationInDps, resources.displayMetrics)
		val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, 0f, translationInPixels)
		return AnimatorSet().apply {
			playTogether(fadeAnimator, translateAnimator)
			duration = 350
		}
	}

    private fun createShakeAnimator(): Animator {
        val animatedView: View = doubleTapCircleView
        val translationInDps = 40f
        val translationInPixels = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, translationInDps, resources.displayMetrics)
        val translationProperty = "translationX"
        var numberOfOscillations = 2
        val transitionSteps: MutableList<Float> = mutableListOf(0f)
        while (numberOfOscillations-- != 0) {
            transitionSteps.add(translationInPixels)
            transitionSteps.add(-translationInPixels)
        }
        transitionSteps.add(0f)
        val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, *transitionSteps.toFloatArray())

        val maxFadeValue = 0.8f
        val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, maxFadeValue, 0f)
        return AnimatorSet().apply {
            playTogether(fadeAnimator, translateAnimator)
            duration = 600
        }
    }

	private fun animateGameOver() {
		val scoreDisappearAnimator: Animator = createScaleScoreAnimator(false).apply {
			withEndAction {
				moveScore(true)
			}
		}
		val scoreAppearAnimator: Animator = createScaleScoreAnimator(true)
		val gameOverCirclesAnimator: Animator = createGameOverCirclesAnimator()
		val scoreAppearGameOverCirclesAnimator = AnimatorSet().apply {
			playTogether(scoreAppearAnimator, gameOverCirclesAnimator)
		}
		val buttonsAnimator: Animator = createButtonsAppearanceAnimator(true)
		AnimatorSet().apply {
			playSequentially(
				scoreDisappearAnimator,
				scoreAppearGameOverCirclesAnimator,
				buttonsAnimator)
			duration = 400
			start()
		}
	}

	private fun createScaleScoreAnimator(appear: Boolean): Animator {
		val endScale: Float = if (appear) 1f else 0f
		return ObjectAnimator.ofPropertyValuesHolder(scoreTextView,
				PropertyValuesHolder.ofFloat(View.SCALE_X, endScale),
				PropertyValuesHolder.ofFloat(View.SCALE_Y, endScale))
	}

	private fun createGameOverCirclesAnimator(): Animator {
		return AnimatorSet().apply {
			playTogether(
				createGameOverCircleAnimator(Action.SWIPE_UP),
				createGameOverCircleAnimator(Action.SWIPE_RIGHT),
				createGameOverCircleAnimator(Action.SWIPE_DOWN),
				createGameOverCircleAnimator(Action.SWIPE_LEFT)
			)
		}
	}

	private fun animateRestart() {
		val scoreDisappearAnimator: Animator = createScaleScoreAnimator(false)
		val restartCirclesAnimator: Animator = createRestartCirclesAnimator()
		val scoreCirclesAnimator = AnimatorSet().apply {
			playTogether(scoreDisappearAnimator, restartCirclesAnimator)
		}
		val buttonsDisappearAnimator: Animator = createButtonsAppearanceAnimator(false)
		AnimatorSet().apply {
			playSequentially(buttonsDisappearAnimator, scoreCirclesAnimator)
			withEndAction {
				moveScore(false)
				restart()
			}
			duration = 400
			start()
		}
	}

	private fun moveScore(toCenter: Boolean) {
		val set = ConstraintSet().apply {
			clone(mgplayConstraintLayout)
			applyTo(mgplayConstraintLayout)
		}
		if (toCenter) {
			with (set) {
				connect(R.id.scoreTextView, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
				connect(R.id.scoreTextView, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
			}
		} else {
			with (set) {
				connect(R.id.scoreTextView, ConstraintSet.TOP, R.id.topGuideline, ConstraintSet.BOTTOM)
				clear(R.id.scoreTextView, ConstraintSet.BOTTOM)
			}
		}
		set.applyTo(mgplayConstraintLayout)
	}

	private fun createRestartCirclesAnimator(): Animator {
		return AnimatorSet().apply {
			playTogether(
				createRestartCircleAnimator(Action.SWIPE_UP),
				createRestartCircleAnimator(Action.SWIPE_RIGHT),
				createRestartCircleAnimator(Action.SWIPE_DOWN),
				createRestartCircleAnimator(Action.SWIPE_LEFT)
			)
		}
	}

	private fun createRestartCircleAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.SWIPE_UP -> swipeUpCircleView
			Action.SWIPE_RIGHT -> swipeRightCircleView
			Action.SWIPE_DOWN -> swipeDownCircleView
			Action.SWIPE_LEFT -> swipeLeftCircleView
			else -> throw IllegalArgumentException("Only swipe actions allowed: $action")
		}

		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 1f, 0f)
		val translation = 0f
		var translationProperty = "translationX"
		if (action == Action.SWIPE_UP || action == Action.SWIPE_DOWN) {
			translationProperty = "translationY"
		}
		val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, translation)
		return AnimatorSet().apply {
			playTogether(fadeAnimator, translateAnimator)
		}
	}

	private fun createGameOverCircleAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.SWIPE_UP -> swipeUpCircleView
			Action.SWIPE_RIGHT -> swipeRightCircleView
			Action.SWIPE_DOWN -> swipeDownCircleView
			Action.SWIPE_LEFT -> swipeLeftCircleView
			else -> throw IllegalArgumentException("Only swipe actions allowed: $action")
		}

		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, 1f)
		var translationInDps = 50f
		if (action == Action.SWIPE_LEFT || action == Action.SWIPE_UP) {
			translationInDps *= -1
		}
		var translationProperty = "translationX"
		if (action == Action.SWIPE_UP || action == Action.SWIPE_DOWN) {
			translationProperty = "translationY"
		}
		val translationInPixels = TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP, translationInDps, resources.displayMetrics)
		val translateAnimator = ObjectAnimator.ofFloat(animatedView, translationProperty, 0f, translationInPixels)
		return AnimatorSet().apply {
			playTogether(fadeAnimator, translateAnimator)
		}
	}

	private fun createTapAnimator(action: Action): Animator {
		val animatedView: View = when (action) {
			Action.TAP -> tapCircleView
			Action.DOUBLE_TAP -> doubleTapCircleView
			else -> throw IllegalArgumentException("Only tap actions allowed: $action")
		}
		val maxFadeValue = 0.8f
		val fadeAnimator = ObjectAnimator.ofFloat(animatedView, View.ALPHA, 0f, maxFadeValue, 0f)

		val initialScale = 0.5f
		val finalScale = 1.5f
		val scaleAnimator = ObjectAnimator.ofPropertyValuesHolder(animatedView,
			PropertyValuesHolder.ofFloat(View.SCALE_X, initialScale, finalScale),
			PropertyValuesHolder.ofFloat(View.SCALE_Y, initialScale, finalScale))

		val singleTapAnimator = AnimatorSet().apply {
			playTogether(fadeAnimator, scaleAnimator)
		}
		val tapAnimator: Animator =  when (action) {
			Action.TAP -> singleTapAnimator
			else -> {
				AnimatorSet().apply {
					playSequentially(singleTapAnimator, singleTapAnimator.clone())
				}
			}
		}
		tapAnimator.duration = 150
		return tapAnimator
	}

	private fun createButtonsAppearanceAnimator(appear: Boolean): Animator {
		val playAgainScaleAnimator: Animator = createScaleAnimator(playAgainImageView, appear)
		val homeScaleAnimator: Animator = createScaleAnimator(homeImageView, appear)
		return AnimatorSet().apply {
			playTogether(playAgainScaleAnimator, homeScaleAnimator)
			if (appear) {
				withStartAction {
					playAgainImageView.visibility = View.VISIBLE
					homeImageView.visibility = View.VISIBLE
				}
			} else {
				withEndAction {
					playAgainImageView.visibility = View.GONE
					playAgainImageView.visibility = View.GONE
				}
			}
		}
	}

	private fun createScaleAnimator(animatedView: View, appear: Boolean): Animator {
		val startValue: Float = if (appear) 0f else 1f
		val endValue: Float = 1 - startValue
		return ObjectAnimator.ofPropertyValuesHolder(animatedView,
				PropertyValuesHolder.ofFloat(View.SCALE_X, startValue, endValue),
				PropertyValuesHolder.ofFloat(View.SCALE_Y, startValue, endValue))
	}

//	tts

	private fun setupTts() {
		val checkLanguageResourcesIntent = Intent().apply {
			action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
		}
		startActivityForResult(checkLanguageResourcesIntent, REQUEST_CODE_CHECK_TTS)
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		when (requestCode) {
			REQUEST_CODE_CHECK_TTS -> {
				if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
					tts = TextToSpeech(this, this)
				} else {
					val installIntent = Intent().apply {
						action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
					}
					startActivity(installIntent)
				}
			}
		}
	}

	override fun onInit(status: Int) {
		val locale: Locale = Locale.getDefault()
		when (tts.isLanguageAvailable(locale)) {
			TextToSpeech.LANG_NOT_SUPPORTED -> {
				Toast.makeText(this, "${locale.displayLanguage} is not available for text-to-speech", Toast.LENGTH_SHORT).show()
			}
			TextToSpeech.LANG_MISSING_DATA -> {
				Toast.makeText(this, "Text-to-speech resources are not installed", Toast.LENGTH_SHORT).show()
			}
			else -> {
				tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
					override fun onDone(utteranceId: String?) {
						runOnUiThread {
							if (utteranceIsInstruction) {
								if (instructionQueue.isNotEmpty()) {
									sayNextInstruction()
								} else {
									onFinishInstructions()
								}
							} else if (memoryGame.isGameOver) {
								animateGameOver()
							}
						}
					}

					@Suppress("OverridingDeprecatedMember")
					override fun onError(utteranceId: String?) {}

					override fun onStart(utteranceId: String?) {
						runOnUiThread {
							if (utteranceIsInstruction && actionToAnimate != null) {
								animateAction(actionToAnimate!!, true)
								actionToAnimate = null
							}
						}
					}
				})
				startGame()
			}
		}
	}

	private fun sayText(command: String, isInstruction: Boolean) {
		utteranceIsInstruction = isInstruction
		val params = HashMap<String, String>()
		params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = command
		@Suppress("DEPRECATION")
		tts.speak(command, TextToSpeech.QUEUE_FLUSH, params)
	}

	private fun stopCommandUtterance() {
		if (::tts.isInitialized) {
			tts.stop()
		}
	}

	private fun removeHandlerCallbacks() {
		handler.removeCallbacksAndMessages(null)
	}

//	Gesture listeners

	private fun onActionEvent(action: Action) {
		if (!isExecuting) return
		if(action!=Action.SHAKE){
			playActionSound(action)
		}
		when (action) {
			Action.SWIPE_UP -> onSwipeUp()
			Action.SWIPE_RIGHT -> onSwipeRight()
			Action.SWIPE_DOWN -> onSwipeDown()
			Action.SWIPE_LEFT -> onSwipeLeft()
			Action.TAP -> onTap()
			Action.DOUBLE_TAP -> onDoubleTap()
			Action.SHAKE -> onShake()
			Action.FACE_DOWN -> onFaceDown()
		}
	}

	private fun onSwipeUp() {
		executeAction(Action.SWIPE_UP)
	}

	private fun onSwipeRight() {
		executeAction(Action.SWIPE_RIGHT)
	}

	private fun onSwipeDown() {
		executeAction(Action.SWIPE_DOWN)
	}

	private fun onSwipeLeft() {
		executeAction(Action.SWIPE_LEFT)
	}

	private fun onTap() {
		executeAction(Action.TAP)
	}

	private fun onDoubleTap() {
		executeAction(Action.DOUBLE_TAP)
	}

	private fun onShake() {
		executeAction(Action.SHAKE)
	}

	private fun onFaceDown(){
		executeAction(Action.FACE_DOWN)
	}



//	Activity lifecycle methods

	override fun onPause() {
		if (allSet){
			stopCommandUtterance()
			removeHandlerCallbacks()
			stopSensey()
			stopBackgroundTrack()
			releaseMediaPlayers()
			allSet=false
		}

		super.onPause()
	}

	private fun releaseMediaPlayers() {


		if ((::swipeSound.isInitialized)){
			swipeSound.release()
		}
		if(::tapSound.isInitialized){
			tapSound.release()
		}
		if(::shakeSound.isInitialized){
			shakeSound.release()
		}



		if(::pianoShake.isInitialized){
			pianoShake.release()
		}
		if(::pianoUp.isInitialized){
			pianoUp.release()
		}
		if(::pianoRight.isInitialized){
			pianoRight.release()
		}





		if(::pianoDown.isInitialized){
			pianoDown.release()
		}

		if(::pianoLeft.isInitialized){
			pianoLeft.release()
		}

		if(::pianoTap.isInitialized){
			pianoTap.release()
		}




		if(::backgroundTrack.isInitialized){
			backgroundTrack.release()
		}

		if(::successSound.isInitialized){
			successSound.release()
		}
		if(::gameOverSound.isInitialized){
			gameOverSound.release()
		}
		if(::faceDownSound.isInitialized) {
			faceDownSound.release()
		}


		if(::congaDown.isInitialized){
			congaDown.release()
		}
		if(::congaLeft.isInitialized){
			congaLeft.release()
		}
		if(::congaRight.isInitialized){
			congaRight.release()
		}
		if(::congaUp.isInitialized){
			congaUp.release()
		}
		if(::congaTap.isInitialized){
			congaTap.release()
		}
		if(::congaShake.isInitialized){
			congaShake.release()
		}

	}

	private fun stopBackgroundTrack() {
		if(::backgroundTrack.isInitialized){
			if (backgroundTrack.isPlaying){
				try {
					backgroundTrack.stop()
				}catch (e: IllegalStateException){
					Log.d(TAG,"wasn't initialized")
				}
			}

		}


	}

	override fun onDestroy() {
		with (tts) {
			stop()
			shutdown()
		}
		super.onDestroy()
	}
}
