package com.team6moviles.ui

class CommunicationMessageException(message: String = "") : Exception(message)