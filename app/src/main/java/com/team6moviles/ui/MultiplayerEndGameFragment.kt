package com.team6moviles.ui

import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.team6moviles.R
import com.team6moviles.model.Player
import com.team6moviles.model.PlayersUtils
import kotlinx.android.synthetic.main.fragment_multiplayer_end_game.*

class MultiplayerEndGameFragment : Fragment() {
	interface FragmentInteractionListener

	private var listener: FragmentInteractionListener? = null

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_multiplayer_end_game, container, false)
	}

	override fun onAttach(context: Context) {
		super.onAttach(context)
		listener = context as? FragmentInteractionListener
	}

	override fun onDetach() {
		super.onDetach()
		listener = null
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        updateLayoutIfRankingUpdate(false)
    }

	fun onRankingUpdate(players: List<Player>, username: String) {
		updateRankingList(players)
		updateRankingPosition(players, username)
        updateLayoutIfRankingUpdate(true)
	}

    private fun updateLayoutIfRankingUpdate(rankingUpdate: Boolean) {
        val rootConstraintSet = ConstraintSet().apply {
            clone(endGameRootConstraintLayout)
        }
        if (rankingUpdate) {
            rootConstraintSet.clear(R.id.endGameTitleTextView, ConstraintSet.BOTTOM)
        } else {
            rootConstraintSet.connect(R.id.endGameTitleTextView, ConstraintSet.BOTTOM,
                ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
        }
        rootConstraintSet.applyTo(endGameRootConstraintLayout)
    }

	private fun updateRankingList(players: List<Player>) {
		val sortedRanking: List<Player> = PlayersUtils.sortByScore(players)
		val rankingStringArray: Array<String> = sortedRanking.mapIndexed {
				index: Int, player: Player ->
			if (player.hasLost) {
				activity!!.getString(R.string.rankingItemFinishedPlayingPlaceholder)
					.format(index + 1, player.username, player.score)
			} else {
				activity!!.getString(R.string.rankingItemStillPlayingPlaceholder)
					.format(index + 1, player.username, player.score)
			}
		}.toTypedArray()
		val arrayAdapter = ArrayAdapter<String>(activity!!, android.R.layout.simple_list_item_1, rankingStringArray)
		rankingListView.adapter = arrayAdapter
	}

	private fun updateRankingPosition(players: List<Player>, username: String) {
		val rankingPositionString: String = PlayersUtils.getRankingPositionString(activity!!, players, username) ?: ""
		rankingPositionTextView.text = rankingPositionString
	}
}