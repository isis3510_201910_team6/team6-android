package com.team6moviles.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.team6moviles.R
import com.team6moviles.model.ClientGame
import com.team6moviles.model.ConnectivityManager
import com.team6moviles.model.MessageManager
import com.team6moviles.model.Player
import com.team6moviles.model.ReceiveThread
import kotlinx.android.synthetic.main.activity_join_game.connectButton
import kotlinx.android.synthetic.main.activity_join_game.ipEditText
import kotlinx.android.synthetic.main.activity_join_game.joinGameRootLayout
import java.lang.IllegalArgumentException
import java.net.Socket

class JoinGameActivity : AppCompatActivity() {

	companion object {
		const val USERNAME_EXTRA = "username_extra"
	}

	private lateinit var handler: Handler
	private lateinit var username: String

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_join_game)

		setupUsername()
		setupHandler()
		setupButtons()
	}

	private fun setupUsername() {
		username = intent.getStringExtra(USERNAME_EXTRA) ?:
				throw IllegalArgumentException("Username extra required")
	}

	private fun setupHandler() {
		handler = Handler(Handler.Callback { msg: Message ->
			when (msg.what) {
				ConnectivityManager.SOCKET -> {
					val socket: Socket = msg.obj as Socket
					onConnectedToServer(socket)
				}
				ConnectivityManager.ERROR_MESSAGE -> {
					val errorMessage: String = msg.obj as String
					Snackbar.make(joinGameRootLayout, errorMessage, Snackbar.LENGTH_INDEFINITE).apply {
						setAction("OK") {
							dismiss()
						}
						show()
					}
					connectButton.isEnabled = true
				}
			}
			true
		})
	}

	private fun onConnectedToServer(socket: Socket) {
		Toast.makeText(this, "Connected to server", Toast.LENGTH_SHORT).show()
		ClientGame.socket = socket
		ClientGame.player = Player(username)

		sendUsernameToServer()
		connectButton.isEnabled = true
		startPlayerLobbyActivity()
	}

	private fun sendUsernameToServer() {
		val usernameMessage: String = MessageManager.createPlayerUsernameMessage(username)
		ConnectivityManager.sendMessage(ClientGame.socket, usernameMessage)
	}

	private fun startPlayerLobbyActivity() {
		val intent = Intent(this, PlayerLobby::class.java)
		startActivity(intent)
	}

	private fun setupButtons() {
		connectButton.setOnClickListener {
			attemptConnection()
		}
	}

	private fun attemptConnection() {
		val ip: String = ipEditText.text.toString()
		connectButton.isEnabled = false
		ConnectivityManager.connectToServer(ip, handler)
	}
}
