package com.team6moviles.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.team6moviles.R
import kotlinx.android.synthetic.main.activity_mpmenu.mpMenuCreateGameButton
import kotlinx.android.synthetic.main.activity_mpmenu.mpMenuJoinGameButton

class MPMenuActivity : AppCompatActivity() {
	companion object {
		const val USERNAME_EXTRA = "username_extra"
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_mpmenu)

		setupButtons()
	}

	private fun setupButtons() {
		mpMenuCreateGameButton.setOnClickListener { createGame() }
		mpMenuJoinGameButton.setOnClickListener { joinGame() }
	}

	private fun createGame() {
		val username: String = intent.getStringExtra(USERNAME_EXTRA) ?:
				throw IllegalArgumentException("Username argument required")
		val intent = Intent(this, CreateGameActivity::class.java).apply {
			putExtra(CreateGameActivity.USERNAME_EXTRA, username)
		}
		startActivity(intent)
	}

	private fun joinGame() {
		val username: String = intent.getStringExtra(USERNAME_EXTRA) ?:
				throw IllegalArgumentException("Username argument required")
		val intent = Intent(this, JoinGameActivity::class.java).apply {
			putExtra(JoinGameActivity.USERNAME_EXTRA, username)
		}
		startActivity(intent)
	}
}
