package com.team6moviles.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import com.team6moviles.R
import com.team6moviles.model.*

class PlayerPlayActivity : AppCompatActivity(),
	MultiplayerPlayFragment.FragmentInteractionListener {

	private var multiplayerPlayFragment: MultiplayerPlayFragment? = null
	private var endGameFragment: MultiplayerEndGameFragment? = null

	private lateinit var handler: Handler
	private var receiveThread: ReceiveThread? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_player_play)

		setupHandler()
		listenForStatusUpdates()
		setMultiplayerPlayFragment()
	}

	private fun setupHandler() {
		handler = Handler(Handler.Callback { msg: Message ->
			when (msg.what) {
				ConnectivityManager.REMOTE_MESSAGE -> {
					val playerMessage: PlayerMessage = msg.obj as PlayerMessage
					processMessage(playerMessage.message)
				}
			}
			true
		})
	}

	private fun processMessage(message: String) {
		val command: MessageManager.Command = MessageManager.getCommand(message)
		when (command) {
			MessageManager.Command.PLAYERS_STATUS -> onPlayersStatusReceived(message)
		}
	}

	private fun onPlayersStatusReceived(message: String) {
		val players: List<Player> = MessageManager.getPlayersStatus(message)
		ClientGame.updatePlayersStatus(players)
		updatePlayersStatusList()
	}

	private fun updatePlayersStatusList() {
		val players: List<Player> = ClientGame.players
		multiplayerPlayFragment?.onPlayersStatusUpdate(players, ClientGame.player.username)
		endGameFragment?.onRankingUpdate(players, ClientGame.player.username)
	}

	private fun listenForStatusUpdates() {
		receiveThread = ConnectivityManager.listenForMessages(ClientGame.socket, handler)
	}

	private fun increaseScore() {
		ClientGame.increaseScore()
	}

	private fun fail() {
		ClientGame.fail()
		setEndGameFragment()
	}

	private fun setEndGameFragment() {
		endGameFragment = MultiplayerEndGameFragment()
		setFragment(endGameFragment!!)
		multiplayerPlayFragment = null
	}

	private fun setMultiplayerPlayFragment() {
		multiplayerPlayFragment = MultiplayerPlayFragment()
		setFragment(multiplayerPlayFragment!!)
	}

	private fun setFragment(fragment: Fragment) {
		supportFragmentManager.beginTransaction().apply {
			setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
			replace(R.id.fragmentContainer, fragment)
			commit()
		}
	}

	override fun onScoreUpdate(score: Int) {
		increaseScore()
	}

	override fun onGameOver() {
		fail()
	}

	override fun onTouchEvent(event: MotionEvent): Boolean {
		if (multiplayerPlayFragment == null) return super.onTouchEvent(event)
		return if (multiplayerPlayFragment!!.onTouchEvent(event)) {
			true
		} else {
			super.onTouchEvent(event)
		}
	}

    private fun startMenuActivity() {
        val intent = Intent(this, MGMenuActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        startMenuActivity()
    }
}
