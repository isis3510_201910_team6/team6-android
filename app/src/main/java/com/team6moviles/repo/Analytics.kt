package com.team6moviles.repo

import android.content.Context
import android.os.Bundle
import com.google.firebase.Timestamp
import com.google.firebase.analytics.FirebaseAnalytics

class Analytics(context: Context){


	/**
	 * The `FirebaseAnalytics` used to record screen views.
	 */
	// [START declare_analytics]
	private  var firebaseAnalytics = FirebaseAnalytics.getInstance(context)


	fun triggerGameFinished(
		game: HashMap<String, Any>,
		highscore: Int
	) {

		val params = Bundle().apply {
			putInt(Firestore.SCORE, game[Firestore.SCORE] as Int)
			putInt(Firestore.DURATION, game[Firestore.DURATION] as Int)
			putLong(Firestore.TIMESTAMP, (game[Firestore.TIMESTAMP] as Timestamp).toDate().time)
			putInt(Firestore.HIGH,highscore)
			putDouble("averageTimeBetweenReactions", 2.4)
		}
		firebaseAnalytics.logEvent("game_finished",params)



	}

}