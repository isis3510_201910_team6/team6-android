package com.team6moviles.repo

import android.content.Context
import android.util.Log
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.team6moviles.model.Action

class Firestore {

	private val TAG = javaClass.simpleName
	companion object {
		const val PLAYERS = "Players"
		const val GAMES = "games"
		const val HIGH = "highscore"
		const val USERNAME = "username"
		const val SEQUENCE = "sequence"
		const val DURATION = "duration"
		const val TIMESTAMP = "timestamp"
		const val SCORE = "score"
	}

	val playersRef = FirebaseFirestore.getInstance().collection(PLAYERS)

	val currentTimestamp: Timestamp
		get() = Timestamp.now()

	fun highscore(callback: (Int) -> Unit) {
		playersRef
			.document(LocalStorage.userId)
			.get()
			.addOnSuccessListener { document ->
				if (document != null) {
					val highscore: Int = (document.data!![HIGH] as Long).toInt()
					callback(highscore)
				} else {
					Log.d(TAG, "No such document")
				}
			}
			.addOnFailureListener { exception ->
				Log.d(TAG, "get failed with ", exception)
			}
	}

	fun getUserDocumentData(callback: (Int, String) -> Unit){
		val docRef = playersRef.document(LocalStorage.userId)

		docRef.get().addOnSuccessListener {
			if (it.exists() && it != null) {
				callback((it.data!![HIGH] as Long).toInt(), it.data!![USERNAME] as String)
				Log.d(TAG, "Current data: ${it.data}")
			} else {
				Log.d(TAG, "Current data: null")
			}
		}.addOnFailureListener{
			Log.w(TAG, "Listen failed.", it)
			return@addOnFailureListener
		}
//		docRef.addSnapshotListener(EventListener<DocumentSnapshot> { snapshot, e ->
//			if (e != null) {
//				Log.w(TAG, "Listen failed.", e)
//				return@EventListener
//			}
//
//			if (snapshot != null && snapshot.exists()) {
//
//				callback((snapshot.data!![HIGH] as Long).toInt(),snapshot.data!![USERNAME] as String)
//				Log.d(TAG, "Current data: ${snapshot.data}")
//			} else {
//				Log.d(TAG, "Current data: null")
//			}
//		})
	}





	fun addGame(game: HashMap<String, Any>) {
		playersRef
			.document(LocalStorage.userId)
			.collection(GAMES)
			.add(game)
	}

	fun toIntArray(actionSequence: List<Action>): ArrayList<Int> {
		val sequence: List<Int> = actionSequence.map {
			action: Action ->
			when (action) {
				Action.SWIPE_UP -> 0
				Action.SWIPE_RIGHT -> 1
				Action.SWIPE_DOWN -> 2
				Action.SWIPE_LEFT -> 3
				Action.TAP -> 4
				Action.DOUBLE_TAP -> 5
				Action.SHAKE -> 6
				Action.FACE_DOWN -> 7
			}
		}
		return ArrayList(sequence)
	}

	fun updateHighscore(highscore: Int) {
		val userRef = playersRef.document(LocalStorage.userId)

		userRef
			.update("highscore", highscore)
			.addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated! Highscore = $highscore") }
			.addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }

	}

	fun foundPlayer( username: String, callback: (Boolean) -> Unit){
		if (username.equals("")) throw IllegalArgumentException("You cannot look for an empty string")
		playersRef
				.whereEqualTo(USERNAME, username).get().addOnSuccessListener { result ->
					var aux = result.documents.size > 0
					Log.d(TAG, "Encontro: $aux y size: ${result.documents.size} ")
					callback(aux)
				}
	}


	fun createPlayer(context: Context, username: String, callback: (Boolean) -> Unit) {
		val player: MutableMap<String, Any> = mutableMapOf(
			HIGH to 0,
			USERNAME to username
		)

		playersRef
			.add(player)
			.addOnSuccessListener { documentReference ->
				Log.d(TAG, "DocumentSnapshot written with ID: ${documentReference.id}")
				LocalStorage.saveToSharedPreferences(context, documentReference.id)
				callback(true)
			}
			.addOnFailureListener { e ->
				Log.w(TAG, "Error adding document", e)
			}
	}
}