package com.team6moviles.repo

import android.content.Context
import com.team6moviles.R

object LocalStorage {

	lateinit var userId: String
		private set
	var loggedIn = false
		private set

	fun saveToSharedPreferences(context: Context, userId: String) {
		val sharedPreferences = context.getSharedPreferences(
			context.getString(R.string.sharedPreferencesKey), Context.MODE_PRIVATE)
		with (sharedPreferences.edit()) {
			putString(context.getString(R.string.key_user_id), userId)
			apply()
		}
		this.userId = userId
	}

	fun logInSharedPreferences(context: Context){
		val sharedPreferences = context.getSharedPreferences(
			context.getString(R.string.sharedPreferencesKey), Context.MODE_PRIVATE)
		with (sharedPreferences.edit()) {
			putBoolean(context.getString(R.string.loggedInKey), true)
			apply()
		}
		this.loggedIn = true
	}

	fun cachePersistedUserId(context: Context): Boolean {
		val sharedPreferences = context.getSharedPreferences(
			context.getString(R.string.sharedPreferencesKey), Context.MODE_PRIVATE)
		val defaultValue = "default_value"
		val userId: String = sharedPreferences.getString(
			context.getString(R.string.key_user_id), defaultValue) as String
		return if (userId != defaultValue) {
			this.userId = userId
			true
		} else {
			false
		}
	}

	fun userLoggedIn(context: Context): Boolean {
		val sharedPreferences = context.getSharedPreferences(
			context.getString(R.string.sharedPreferencesKey), Context.MODE_PRIVATE)
		val defaultValue = false
		val logged: Boolean = sharedPreferences.getBoolean(
			context.getString(R.string.loggedInKey), defaultValue) as Boolean
		return if (logged != defaultValue) {
			this.loggedIn = logged
			true
		} else {
			false
		}
	}

}