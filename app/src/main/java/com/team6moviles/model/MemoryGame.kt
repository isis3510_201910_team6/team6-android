package com.team6moviles.model

class MemoryGame(numberOfActions: Int) {
	private val actionsBag: Set<Action> = when (numberOfActions) {
		1 -> setOf(Action.SHAKE)
		2 -> setOf( Action.SHAKE, Action.FACE_DOWN)
		4 -> setOf(Action.SWIPE_UP, Action.SWIPE_RIGHT, Action.SWIPE_DOWN, Action.SWIPE_LEFT)
		5 -> setOf(Action.SWIPE_UP, Action.SWIPE_RIGHT, Action.SWIPE_DOWN, Action.SWIPE_LEFT, Action.TAP)
		6 -> setOf(Action.SWIPE_UP, Action.SWIPE_RIGHT, Action.SWIPE_DOWN, Action.SWIPE_LEFT, Action.TAP, Action.SHAKE)
		7 -> setOf(Action.SWIPE_UP, Action.SWIPE_RIGHT, Action.SWIPE_DOWN, Action.SWIPE_LEFT, Action.TAP, Action.SHAKE, Action.FACE_DOWN)
		else -> throw IllegalArgumentException("Invalid number of actions: $numberOfActions")
	}

	private val sequence: MutableList<Action> = mutableListOf()

	val actionSequence: List<Action>
		get() = sequence.toList()

	private val lastPosition: Int
		get() = sequence.size - 1

	val score: Int
		get() = lastPosition

	private var guessingPosition = 0

	var isGameOver = false

	val isReadyForNextSequence: Boolean
		get() = guessingPosition == 0 && !isGameOver

	init { setGameInitialState() }

	private fun setGameInitialState() {
		sequence.clear()
		addActionToSequence()
		guessingPosition = 0
		isGameOver = false
	}

	private fun addActionToSequence() = sequence.add(actionsBag.random())

	fun executeAction(executedAction: Action): Boolean? {
		if (isGameOver) throw IllegalStateException("The game is already over")
		val correctAction: Action = sequence[guessingPosition]
		val isCorrect: Boolean = executedAction == correctAction
		if(executedAction == Action.SHAKE && !isCorrect)
			return null
		if (isCorrect) {
			if (guessingPosition != lastPosition) {
				guessingPosition++
			} else {
				addActionToSequence()
				guessingPosition = 0
			}
		} else {
			isGameOver = true
		}
		return isCorrect
	}

	fun reset() = setGameInitialState()
}

/*
fun main() {
	print("Number of actions: ")
	val numberOfActions: Int = readLine()!!.toInt()
	val game = MemoryGame(numberOfActions)
	while (true) {
		println("\nScore: ${game.score}")
		println(game.actionSequence)
		println("Enter the sequence:\n")
		var numberOfActionsInSequence: Int = game.actionSequence.size
		while (numberOfActionsInSequence-- != 0) {
			var action: String
			var isActionRecognized: Boolean
			do {
				print("Enter action: ")
				action = readLine()!!
				isActionRecognized = action in "u r d l t".split(" ")
				if (!isActionRecognized) {
					println("Command not recognized")
				}
			} while (!isActionRecognized)
			val isCorrect: Boolean = when (action) {
				"u" -> game.executeAction(Action.SWIPE_UP)
				"r" -> game.executeAction(Action.SWIPE_RIGHT)
				"d" -> game.executeAction(Action.SWIPE_DOWN)
				"l" -> game.executeAction(Action.SWIPE_LEFT)
				"t" -> game.executeAction(Action.TAP)
				else -> throw IllegalArgumentException()
			}
			if (!isCorrect) {
				println("Incorrect!")
			}
		}
	}
}*/
