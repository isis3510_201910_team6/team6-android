package com.team6moviles.model

import android.os.Handler
import java.net.ServerSocket
import java.net.Socket

object ConnectivityManager {
	const val PORT = 8888
	const val REMOTE_MESSAGE = 1
	const val SOCKET = 2
	const val ERROR_MESSAGE = 3

	fun waitForClients(serverSocket: ServerSocket, handler: Handler) {
		ServerThread(serverSocket, handler).start()
	}

	fun listenForMessages(socket: Socket, handler: Handler): ReceiveThread {
		val receiveThread = ReceiveThread(socket, handler)
		receiveThread.start()
		return receiveThread
	}

	fun connectToServer(ipAddress: String, handler: Handler) {
		ClientThread(ipAddress, handler).start()
	}

	fun sendMessage(socket: Socket, message: String) {
		SendThread(socket, message).start()
	}
}
