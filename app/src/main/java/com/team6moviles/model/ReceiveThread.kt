package com.team6moviles.model

import android.os.Handler
import android.util.Log
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.Socket
import java.net.SocketException

class ReceiveThread(private val socket: Socket, private val handler: Handler) : Thread() {
	override fun run() {
//		val br = BufferedReader(InputStreamReader(socket.getInputStream()))
//		while (true) {
//			try {
//				val receivedMessage: String? = br.readLine()
//				if (receivedMessage != null) {
//					val playerMessage = PlayerMessage(receivedMessage, socket)
//					handler.obtainMessage(ConnectivityManager.REMOTE_MESSAGE, playerMessage).sendToTarget()
//				} else {
//					Log.d("ReceiveThread", "Other socket closed")
//					break
//				}
//			} catch (socketException : SocketException) {
//				Log.d("ReceiveThread", "This socket closed")
//				break
//			}
//		}
        val inputStream: InputStream = socket.inputStream
        val buffer = ByteArray(1024)
        var bytes: Int
        while (true) {
            if (Thread.interrupted()) break
            try {
                bytes = inputStream.read(buffer)
                if (bytes > 0) {
                    val receivedMessage = String(buffer, 0, bytes)
                    val playerMessage = PlayerMessage(receivedMessage, socket)
                    handler.obtainMessage(ConnectivityManager.REMOTE_MESSAGE, playerMessage).sendToTarget()
                }
            } catch (socketException: SocketException) {
//                Socket closed
                Log.d("ReceivedThread", socketException.message)
                break
            }
        }
	}
}
