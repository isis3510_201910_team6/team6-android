package com.team6moviles.model

import android.os.Handler
import java.net.InetSocketAddress
import java.net.Socket

class ClientThread(private val ipAddress: String, private val handler: Handler) : Thread() {

	override fun run() {
		try {
			val socket = Socket().apply {
				connect(InetSocketAddress(ipAddress, ConnectivityManager.PORT), 5000)
			}
			handler.obtainMessage(ConnectivityManager.SOCKET, socket).sendToTarget()
		} catch (e: Exception) {
			handler.obtainMessage(ConnectivityManager.ERROR_MESSAGE, "Couldn't connect to server, check the ip address").sendToTarget()
		}
	}
}