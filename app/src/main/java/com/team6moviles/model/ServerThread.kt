package com.team6moviles.model

import android.os.Handler
import android.util.Log
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException

class ServerThread(private val serverSocket: ServerSocket, private val handler: Handler): Thread() {
	override fun run() {
		serverSocket.use {
				serverSocket ->
			while (true) {
				try {
					val socket: Socket = serverSocket.accept()
					handler.obtainMessage(ConnectivityManager.SOCKET, socket).sendToTarget()
				} catch (socketException: SocketException) {
					Log.d("ServerThread", "This server socket closed")
					break
				}
			}
		}
	}
}