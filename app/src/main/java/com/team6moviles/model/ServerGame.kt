package com.team6moviles.model

import android.os.Handler

object ServerGame {
	lateinit var player: Player
	private val remotePlayers: MutableList<RemotePlayer> = mutableListOf()

	val players: List<Player>
		get() {
			val players = remotePlayers
				.map(RemotePlayer::player)
				.toMutableList()
			players.add(player)
			return players
		}
	val playersUsernames: List<String>
		get() = players.map(Player::username)

	fun startGame() {
		val startGameMessage: String = MessageManager.createStartGameMessage()
		ServerGame.broadcastMessage(startGameMessage)
	}

	fun updatePlayerStatus(updatedPlayer: Player) {
		val players: List<Player> = remotePlayers.map(RemotePlayer::player)
		for (player in players) {
			if (player.username == updatedPlayer.username) {
				player.updateFromPlayer(updatedPlayer)
				broadcastStatus()
				return
			}
		}
	}

	fun listenToClients(handler: Handler) {
		for (remotePlayer in remotePlayers) {
			ConnectivityManager.listenForMessages(remotePlayer.socket, handler)
		}
	}

	fun increaseScore() {
		player.increaseScore()
		broadcastStatus()
	}

	fun fail() {
		player.hasLost = true
		broadcastStatus()
	}

	fun addRemotePlayer(remotePlayer: RemotePlayer) {
		remotePlayers.add(remotePlayer)
		val playersUsernamesMessage = MessageManager.createPlayersUsernamesMessage(ServerGame.playersUsernames)
		ServerGame.broadcastMessage(playersUsernamesMessage)
	}

	private fun broadcastMessage(message: String) {
		for (remotePlayer in remotePlayers) {
			ConnectivityManager.sendMessage(remotePlayer.socket, message)
		}
	}

	private fun broadcastStatus() {
		val players: MutableList<Player> = remotePlayers.map(RemotePlayer::player).toMutableList()
		players.add(player)
		val playersStatusMessage = MessageManager.createPlayersStatusMessage(players)
		broadcastMessage(playersStatusMessage)
	}
}