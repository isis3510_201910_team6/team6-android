package com.team6moviles.model

import com.team6moviles.R

enum class Action(
	val commandResId: Int,
	val colorResId: Int) {

	SWIPE_UP(R.string.swipe_up_command, R.color.swipe_up),
	SWIPE_RIGHT(R.string.swipe_right_command, R.color.swipe_right),
	SWIPE_DOWN(R.string.swipe_down_command, R.color.swipe_down),
	SWIPE_LEFT(R.string.swipe_left_command, R.color.swipe_left),
	TAP(R.string.tap_command, R.color.tap),
	DOUBLE_TAP(R.string.double_tap_command, R.color.double_tap),
	SHAKE(R.string.shake,R.color.colorPrimaryLight),
	FACE_DOWN(R.string.face_down,R.color.colorPrimaryLight)
}
