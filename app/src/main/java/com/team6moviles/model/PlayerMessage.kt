package com.team6moviles.model

import java.net.Socket

data class PlayerMessage(val message: String, val socket: Socket)