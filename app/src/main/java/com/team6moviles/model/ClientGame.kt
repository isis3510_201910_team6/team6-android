package com.team6moviles.model

import java.net.Socket

object ClientGame {
	lateinit var player: Player
	lateinit var socket: Socket
	val players: MutableList<Player> = mutableListOf()

	private fun updatePlayerStatus(updatedPlayer: Player) {
		var foundPlayer = false
		for (player in players) {
			if (player.username == updatedPlayer.username) {
				player.updateFromPlayer(updatedPlayer)
				foundPlayer = true
				break
			}
		}
		if (!foundPlayer) {
			players.add(updatedPlayer)
		}
	}

	fun updatePlayersStatus(updatedPlayers: List<Player>) {
		for (updatedPlayer in updatedPlayers) {
			updatePlayerStatus(updatedPlayer)
		}
	}

	fun increaseScore() {
		player.increaseScore()
		sendStatus()
	}

	fun fail() {
		player.hasLost = true
		sendStatus()
	}

	private fun sendStatus() {
		val playerStatusMessage: String = MessageManager.createPlayerStatusMessage(player)
		sendMessage(playerStatusMessage)
	}

	private fun sendMessage(message: String) {
		ConnectivityManager.sendMessage(socket, message)
	}
}