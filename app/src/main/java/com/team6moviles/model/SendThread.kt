package com.team6moviles.model

import java.net.Socket

class SendThread(private val socket: Socket, private val message: String) : Thread() {
	override fun run() {
//		PrintWriter(socket.getOutputStream(), true).println(message)
//		java.net.SocketException: sendto failed: EPIPE (Broken pipe)
//		Caused by: android.system.ErrnoException: sendto failed: EPIPE (Broken pipe)
        socket.outputStream.write(message.toByteArray())
	}
}
