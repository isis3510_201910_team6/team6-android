package com.team6moviles.model

import java.net.Socket

data class RemotePlayer(
	val player: Player,
	val socket: Socket
)