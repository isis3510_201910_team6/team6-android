package com.team6moviles.model

import com.team6moviles.ui.CommunicationMessageException

object MessageManager {
	private const val COMMAND_SEPARATOR = ":"
	private const val PLAYER_SEPARATOR = ";"
	private const val PARAMETER_SEPARATOR = ","

	enum class Command {
		PLAYER_USERNAME,
		PLAYERS_USERNAMES,
		START_GAME,
		PLAYER_STATUS,
		PLAYERS_STATUS
	}

    private fun preprocessMessage(message: String): String {
        return message.trim()
    }

	fun getCommand(m: String): Command {
        val message = preprocessMessage(m)
		val messageArray: List<String> = message.split(COMMAND_SEPARATOR)
		if (messageArray.isEmpty()) throw CommunicationMessageException("No command found: $message")
		val commandStr = messageArray[0]
		for (command: Command in Command.values()) {
			if (command.toString() == commandStr) {
				return command
			}
		}
		throw CommunicationMessageException("Command not recognized: $commandStr")
	}

	fun getPlayerUsername(m: String): String {
        val message = preprocessMessage(m)
		if (getCommand(message) != Command.PLAYER_USERNAME) {
			throw CommunicationMessageException("Message doesn't have the ${Command.PLAYER_USERNAME} command")
		}
		val messageArray: List<String> = message.split(COMMAND_SEPARATOR)
		if (messageArray.size != 2) throw CommunicationMessageException("The message should have 1 parameter")
		return messageArray[1]
	}

	fun getPlayersUsernames(m: String): List<String> {
        val message = preprocessMessage(m)
		if (getCommand(message) != Command.PLAYERS_USERNAMES) {
			throw CommunicationMessageException("Message doesn't have the ${Command.PLAYERS_USERNAMES} command")
		}
		val messageArray: List<String> = message.split(COMMAND_SEPARATOR)
		if (messageArray.size != 2) throw CommunicationMessageException("The message doesn't have a body")
		return messageArray[1].split(PLAYER_SEPARATOR)
	}

	private fun getPlayerFromPlayerParameters(playerParametersStr: String): Player {
		val playerParameters: List<String> = playerParametersStr.split(PARAMETER_SEPARATOR)
		val username: String = playerParameters[0]
		val score: Int = playerParameters[1].toIntOrNull() ?:
		throw CommunicationMessageException("score parameter is not an integer: ${playerParameters[1]}")
		val hasLost: Boolean = when (playerParameters[2]) {
			"true" -> true
			"false" -> false
			else -> throw CommunicationMessageException("hasLost parameter is neither 'true' nor 'false'")
		}
		return Player(username, score, hasLost)
	}

	fun getPlayerStatus(m: String): Player {
        val message = preprocessMessage(m)
		if (getCommand(message) != Command.PLAYER_STATUS) {
			throw CommunicationMessageException("Message doesn't have the ${Command.PLAYER_STATUS} command")
		}
		val messageArray: List<String> = message.split(COMMAND_SEPARATOR)
		if (messageArray.size != 2) throw CommunicationMessageException("The message doesn't have a body")
		return getPlayerFromPlayerParameters(messageArray[1])
	}

	fun getPlayersStatus(m: String): List<Player> {
        val message = preprocessMessage(m)
		if (getCommand(message) != Command.PLAYERS_STATUS) {
			throw CommunicationMessageException("Message doesn't have the ${Command.PLAYERS_STATUS} command")
		}
		val messageArray: List<String> = message.split(COMMAND_SEPARATOR)
		if (messageArray.size != 2) throw CommunicationMessageException("The message doesn't have a body")
		val playersParameters: List<String> = messageArray[1].split(PLAYER_SEPARATOR)
		val players: MutableList<Player> = mutableListOf()
		for (playerParameters in playersParameters) {
			players.add(getPlayerFromPlayerParameters(playerParameters))
		}
		return players
	}

	fun createPlayerUsernameMessage(username: String): String {
		return "" + Command.PLAYER_USERNAME + COMMAND_SEPARATOR + username
	}

	fun createPlayersUsernamesMessage(usernames: List<String>): String {
		var message = "" + Command.PLAYERS_USERNAMES + COMMAND_SEPARATOR
		usernames.forEachIndexed { index, username ->
			message += username
			if (index != usernames.size - 1) {
				message += PLAYER_SEPARATOR
			}
		}
		return message
	}

	fun createStartGameMessage(): String = Command.START_GAME.toString()

	fun createPlayerStatusMessage(player: Player): String {
		return "" + Command.PLAYER_STATUS + COMMAND_SEPARATOR + player.username +
				PARAMETER_SEPARATOR + player.score + PARAMETER_SEPARATOR + player.hasLost
	}

	fun createPlayersStatusMessage(players: List<Player>): String {
		var message = "" + Command.PLAYERS_STATUS + COMMAND_SEPARATOR
		players.forEachIndexed { index, player ->
			message += player.username + PARAMETER_SEPARATOR + player.score + PARAMETER_SEPARATOR + player.hasLost
			if (index != players.size - 1) {
				message += PLAYER_SEPARATOR
			}
		}
		return message
	}
}