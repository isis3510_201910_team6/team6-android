package com.team6moviles.model

data class Player(
	val username: String,
	var score: Int = 0,
	var hasLost: Boolean = false
) {
	fun increaseScore() {
		score++
	}

	fun updateFromPlayer(otherPlayer: Player) {
		this.score = otherPlayer.score
		this.hasLost = otherPlayer.hasLost
	}
}