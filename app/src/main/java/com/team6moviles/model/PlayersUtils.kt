package com.team6moviles.model

import android.content.Context
import com.team6moviles.R

object PlayersUtils {

	private fun getPlayer(players: List<Player>, username: String): Player? {
		for (player in players) {
			if (player.username == username) {
				return player
			}
		}
		return null
	}

	fun sortByScore(players: List<Player>): List<Player> {
		return players.sortedByDescending(Player::score)
	}

	private fun playersRemaining(players: List<Player>): Int {
		return players.count { !it.hasLost }
	}

	private fun getLeader(players: List<Player>): Player? {
		return sortByScore(players).firstOrNull()
	}

	private fun getRankingPositionString(players: List<Player>, username: String): Int? {
		val player: Player = getPlayer(players, username) ?: return null
		sortByScore(players).forEachIndexed {
			index: Int, p: Player ->
			if (player.username == p.username) {
				return index + 1
			}
		}
		return null
	}

	fun getRankingPositionString(context: Context, players: List<Player>, username: String): String? {
		val rankingPosition: Int = getRankingPositionString(players, username) ?: return null
		return context.getString(R.string.rankingPositionPlaceholder).format(rankingPosition)
	}

	fun getLeaderString(context: Context, players: List<Player>): String? {
		val leader: Player = getLeader(players) ?: return null
		return context.getString(R.string.bestPlayerPlaceholder).format(leader.username, leader.score)
	}

	fun getPlayersRemainingString(context: Context, players: List<Player>): String? {
		val playersRemaining: Int = playersRemaining(players)
		return context.getString(R.string.playersRemainingPlaceholder).format(playersRemaining)
	}
}